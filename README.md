# RVision-PTZ-Intelligent

## Using the visual servoing program (LINUX)
```	$>  cd visual_servoing ```

```	$>  make ```

``` $>	sudo ./asservissement camera_input_id```

Where `camera_input_id` should be set to 0 if at least one camera exists and is detected by your computer.

## Visual tracking availables camera modes 
[Camshift.cpp](visual_servoing/src/Camshift.cpp)

[OpticalFlowPyramidal.cpp](visual_servoing/src/OpticalFlowPyramidal.cpp)

## Creating a new camera mode

### Class creation

To create a new camera mode, you will need to create a new class inheriting from ```CameraMode``` class (required opencv includes aren't showed in this example):

```cpp
#include "CameraMode.hpp"
#include "CameraModeFactory.hpp"

class MyNewCameraMode : public CameraMode {
	private:
		using super = CameraMode;
		...

	public:
		MyNewCameraMode( VideoCapture * cap);
		~MyNewCameraMode();
		virtual void exec();   
		virtual void init();
		virtual void help();
		virtual void stop();	
		...
}

```
The public methods in the example above, are actually pure virtual method of the base class ```CameraMode``` and need to be implemented in our newly created class. Lets review this methods.

#### virtual void init();

The initialization of all of the class attributes should be implemented in this method. This method will be called each time our camera mode is requested by the user. It is strongly recommended to also initialize opencv windows in this method.
Lets review an example of this method's body:
```cpp
void MyNewCameraMode::init()
{
	
    if( !this->getCap()->isOpened() ) // Defined in CameraMode.
    {
        cout << "***Could not initialize capturing...***\n";
        return ;
    }

    this->help(); // Short summary displayed in the terminal.
    cout << this->getHot_keys(); // Available keyboard keys displayed in terminal.
    
    namedWindow( "NAME_OF_CVWINDOW", 0 );
    resizeWindow("NAME_OF_CVWINDOW", 800 , 600);
    
    setMouseCallback( "NAME_OF_CVWINDOW", onMouse, 0 );
    createTrackbar( "min", "NAME_OF_CVWINDOW", &vmin, 256, 0 );
    createTrackbar( "max", "NAME_OF_CVWINDOW", &vmax, 256, 0 );
    createTrackbar( "min", "NAME_OF_CVWINDOW", &smin, 256, 0 );
}
```

#### virtual void stop();

In this method, a cleaning and/or  desalocating  process should be implemented. Keep in mind that the mode is never deleted, in other words, ```delete myNewCameraBehavior``` is not called. This mode's reference is rather saved in the ```CameraModeFactory::cameraModesInstaces``` map. This allow for heavy allocated structures in MyNewCameraMode to remain undelete if requested by the user.
Example:
```cpp
void StandBy::stop()
{
  cv::destroyAllWindows();
  delete this->pointer;
}
```

#### virtual void exec();

In this method body's, the behavior of the mode shall be implemented. If the program gets out of this method scope, ```stop()``` is called. Therefore if a looped behavior is desired, it should be placed into this method.
There's no inherited solution to stop the eventually needed loop. It's then the coder's responsibility to implement a way to stop this method. It's suggested to observe the cv::waitKey(x) return value to check if the user has requested to quit the loop.
Once the loop stopped and before exiting ```exec()``` scope's ```CameraModeFactory::get(CameraModeFactory::STANDBY)``` must at least be called to get the StandBy camera mode where all the other available mode can be requested, otherwise the program will execute the last selected camera mode.
An example of this process would be:
```cpp
	while(c != 'x')
    {
    	// Get frame from camera.
        *this->getCap() >> frame;
        if( frame.empty() )
            return;

        // flip(frame,frame,0); 
        super::frameCorrection(&frame);

        // Check for keyboard event.
        c = (char)waitKey(10);

        // Check if it's a request for moving the PTZ camera.
        super::checkKeys(c);

        // Check other request.
        switch(c)
        {
        	...

            case exit_defined_key:
            	// Switching to StandBy mode.
                this->switchCameraMode(CameraModeFactory::STANDBY);
                c= 'x';
                break;
        }
    }
```

#### virtual void help();
This method is called each time a camera mode starts. The purpose of this method is to display instructions for the user. By the time this has been written, it is used to prompt in the terminal all the possibles commands that can be used with eventually, a summary of the requested mode. If another form of displaying the information is available, it should be included into this method.

#### MyNewCameraMode( cv::VideoCapture * cap);
As our new mode will be processing a PTZ camera video input, it's essential that the mode has access to the generated frames. cv::VideoCapture allows visual access to the camera and need to be passed to the base class. An example of the constructor would then be:
```cpp
MyNewCameraMode::MyNewCameraMode( VideoCapture * cap) : CameraMode(cap) 
{
	// A way of saving keyboard keys instructions.
	string hot_keys =
    "\n\nHot keys: \n"
    "\tESC - quit the program\n"
	this->setHot_keys(hot_keys);
}
```

#### ~ MyNewCameraMode();
The destructor will be used to unallocate all that was left alive by the ```stop()``` method.

### Adding created class to factory
The memory management is centralized in the ```CameraModeFactory``` class. Therefore if we want our new camera mode to be used by the program, we need to add some informations to the factory class. 
Into the enums values of CameraModeFactory::CameraModes in [CameraModeFactory.hpp](visual_servoing/src/CameraModeFactory.hpp) add a CHOSED_ID for the newly created camera mode.

For the new camera mode instantiation, in [CameraModeFactory.cpp](visual_servoing/src/CameraModeFactory.cpp) add the next line:
```cpp
	CameraModeFactory::cameraModeInstances->insert((make_pair(CameraModeFactory::CHOSED_ID, new MyNewCameraMode(cap, ...))));
```
### Switching modes
```void CameraMode::switchCameraMode(CameraModeFactory::CameraModes mode)```
The method above allows to switch from camera modes. It must be the last function called from the ```exec()``` scope.
### StandBy mode, the main example
[StandBy.cpp](visual_servoing/src/StandBy.cpp) can be used as a light example of how a full implemented camera mode would look like.

## Using python script files (in /visual_scratches)

For python script files, the command line arguments are handled in the simplest way, each python program takes the first argument as the device name from which you get your image(could be 0, 1, ...). There are two possible ways to execute the .py files:

``` $> python <python_filename> 0 ```
 the last number, here is 0 indicates the device number, 

or by changing the permission of python file, you type the file name to execute the file directly,

``` $> chmod 764 <python_filename> ```

then, you could run the python example files like this:

``` $> ./python_filename.py 0 ```

## Scratches files

### cam_basic_example.py

Example for testing the camera with basic OpenCV filter functions:

+ cv2.Sobel
+ cv2.GaussianBlur
+ cv2.Laplacian
+ cv2.Canny

### cam_bgsub_mog_example.py

Example of background subtraction using the MODG2 method.

+ cv2.createBackgroundSubtractorMOG2

Notice that, camera frame will be fliped.

### cam_sift_example.py

Example of SIFT algorithm.

### image_pyramid.py

Example of image pyramid, by default it will read the [butterfly.jpg](./scratches/imgs/butterfly.jpg) file under folder `imgs`. Use +/- to increase or descrease the level of pyramid.

For more details about image pyramid, look up the chapter 3.5 of [Computer Vision: Algorithms and Applications](https://drive.google.com/open?id=1LsHuONzI6Pdl_I_HpQc8qTbLsGc6KqLw).

### image_pyramid_cam.py

Same with the previous example, except that the image now is a frame of camera. Use +/- to increase or descrease the level of pyramid, SPC to capture the current frame in order to get image pyramid.

For more details about image pyramid, look up the chapter 3.5 of [Computer Vision: Algorithms and Applications](https://drive.google.com/open?id=1LsHuONzI6Pdl_I_HpQc8qTbLsGc6KqLw).
