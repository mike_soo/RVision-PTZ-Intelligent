#!/usr/bin/python

import sys
import cv2

def buildPyramid(img, level, **kwargs):
    pyramid = []
    pyramid.append(img)

    if level >= 0:
        if level == 0:
            return pyramid
        else:
            for i in range(1, level + 1):
                pyramid.append(cv2.pyrDown(pyramid[i - 1]))

            return pyramid


def main(argv):
    device = int(sys.argv[1])
    cam = cv2.VideoCapture(device)
    level = 2
    print("level = " + str(level))
    print("use -/+ to change level")

    while True:
        _, frame = cam.read()

        cv2.imshow("Cam", frame)

        key = cv2.waitKey(5) & 0xff

        if key == 27:
            break;

        elif chr(key) == '+':
            level += 1
            print("level = " + str(level))

        elif chr(key) == '-':
            if level > 0:
                level -= 1
                cv2.destroyWindow("Level" + str(level))
            print("level = " + str(level))

        elif key == 32:
            print('press')
            img_pyramid = buildPyramid(frame, level)

            if img_pyramid:
                for i in range(1, len(img_pyramid)):
                    cv2.imshow('Level' + str(i), img_pyramid[i])

    cam.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main(sys.argv[1:])
