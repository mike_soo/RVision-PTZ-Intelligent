#!/usr/bin/python

import sys
import cv2

device = int(sys.argv[1])

cam = cv2.VideoCapture(device)

while True:
    ret, frame = cam.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    sobelx = cv2.Sobel(gray, cv2.CV_64F, 1, 0, 5)
    sobely = cv2.Sobel(gray, cv2.CV_64F, 0, 1, 5)
    denoised = cv2.GaussianBlur(gray, (5, 5), 0)
    laplacian = cv2.Laplacian(denoised, cv2.CV_64F)
    canny = cv2.Canny(gray, 100, 200)

    cv2.imshow('Original', frame)
    cv2.imshow('Gray', gray)
    cv2.imshow('SobelX', sobelx)
    cv2.imshow('SobelY', sobely)
    cv2.imshow('Laplacian', laplacian)
    cv2.imshow('Canny', canny)

    key = cv2.waitKey(5) & 0xff
    if key == 27:
        break;

cam.release()
cv2.destroyAllWindows()
