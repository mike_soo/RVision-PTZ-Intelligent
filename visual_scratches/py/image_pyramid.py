#!/usr/bin/python

import sys
import cv2

def buildPyramid(img, level, **kwargs):
    pyramid = []
    pyramid.append(img)

    if level >= 0:
        if level == 0:
            return pyramid
        else:
            for i in range(1, level + 1):
                pyramid.append(cv2.pyrDown(pyramid[i - 1]))

            return pyramid


def main(argv):
    filename = argv[0] if len(argv) > 0 else "../imgs/butterfly.jpg"

    img_src = cv2.imread(filename)

    level = 2

    if img_src is None:
        print("Error loading image!")
        exit(-1);

    while True:
        rows, cols, _channels = map(int, img_src.shape)

        img_pyramid = buildPyramid(img_src, level)

        if img_pyramid:
            for i in range(len(img_pyramid)):
                cv2.imshow('Level' + str(i), img_pyramid[i])

        key = cv2.waitKey(5) & 0xff

        if key == 27:
            break;

        elif chr(key) == '+':
            level += 1
            print("level = " + str(level))

        elif chr(key) == '-':
            if level > 0:
                level -= 1
                cv2.destroyWindow("Level" + str(level))
            print("level = " + str(level))


    cv2.destroyAllWindows()

if __name__ == "__main__":
    main(sys.argv[1:])
