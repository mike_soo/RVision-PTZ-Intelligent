#!/usr/bin/python

import sys
import cv2

device = int(sys.argv[1])

cam = cv2.VideoCapture(device)

while True:
    ret, frame = cam.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    sift = cv2.xfeatures2d.SIFT_create()
    kp = sift.detect(gray,None)
    img = cv2.drawKeypoints(gray, kp, frame)
    cv2.imshow('Sift', img);

    key = cv2.waitKey(5) & 0xff
    if key == 27:
        break;

cam.release()
cv2.destroyAllWindows()
