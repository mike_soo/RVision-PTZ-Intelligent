#!/usr/bin/python

import sys
import numpy as np
import cv2

device = int(sys.argv[1])

cam = cv2.VideoCapture(device)

fgbg = cv2.createBackgroundSubtractorMOG2()

while True:
    ret, frame = cam.read()
    frame_img = cv2.flip(frame, 0)

    fgmask = fgbg.apply(frame_img)

    cv2.imshow('frame', fgmask)

    key = cv2.waitKey(5) & 0xff
    if key == 27:
        break;

cam.release()
cv2.destroyAllWindows()

