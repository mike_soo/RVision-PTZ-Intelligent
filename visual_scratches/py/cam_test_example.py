#!/usr/bin/python

import sys
import cv2

device = int(sys.argv[1])

cam = cv2.VideoCapture(device)

while True:
    ret, frame = cam.read()

    cv2.imshow('Frame', frame)

    key = cv2.waitKey(5) & 0xff
    if key == 27:  # ESC
        break

cam.release()
cv2.destroyAllWindows()
