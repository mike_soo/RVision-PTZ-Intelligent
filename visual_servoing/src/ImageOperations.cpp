#include "ImageOperations.hpp"


Mat ImageOperations::rotate(Mat src, double angle)
{
    Mat dst;
    Point2f pt(src.cols/2., src.rows/2.);    
    Mat r = getRotationMatrix2D(pt, angle, 1.0);
    warpAffine(src, dst, r, Size(src.cols, src.rows));
    return dst;
}

void ImageOperations::makeNbOfRowsOdd(Mat * mat)
{
	if(mat->rows % 2 == 0)
	{
		Mat row = Mat::ones(1, mat->cols, mat->type());
		mat->push_back(row);
	}
}

void ImageOperations::makeNbOfColsOdd(Mat * mat)
{
	if(mat->cols % 2 == 0)
	{
		Mat col = Mat::ones(mat->rows , 1 , mat->type());
		cout<<"ImageOperations::makeNbOfColsOdd() : col.size = "
		<<"cols:"<<col.cols		
		<<" rows:"<<col.rows
		<<" col->dim="<<col.dims
		<<" type = "<<col.type()<<endl;
		cout<<"ImageOperations::makeNbOfColsOdd() : mat->size ="
		<< "cols:"<<mat->cols
		<<" rows:"<<mat->rows
		<<" mat->dim="<<mat->dims
		<<" type = "<<mat->type()<<endl; 
		hconcat(*mat, col , *mat);	
	}
}