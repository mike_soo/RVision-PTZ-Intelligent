#include "Camshift.hpp"

Mat image;
bool selectObject = false;
Point origin;
Rect selection;
int trackObject = 0;


/**
 * @brief      Mouse event function callback.
 *
 * @param[in]  event      The mouse event type
 * @param[in]  x          x-axis the mouse event
 * @param[in]  y          y-axis the mouse event
 * @param[in]  <unnamed>  { parameter_description }
 */
static void onMouse( int event, int x, int y, int, void* )
{
    // If a selection is in progress, modify the last selection with the one just detected to create a real time selection size variation.
    if( selectObject )
    {
        selection.x = MIN(x, origin.x);
        selection.y = MIN(y, origin.y);
        selection.width = std::abs(x - origin.x);
        selection.height = std::abs(y - origin.y);

        selection &= Rect(0, 0, image.cols, image.rows);
        
    }

    switch( event )
    {
    // First rectangular selection
    case EVENT_LBUTTONDOWN:
        origin = Point(x,y);
        selection = Rect(x,y,0,0);
        selectObject = true;
        break;
    // Last rectangular selection modification (when the left click button get
    // released)
    case EVENT_LBUTTONUP:
        selectObject = false;
        if( selection.width > 0 && selection.height > 0 )
            trackObject = -1;   // Set up CAMShift properties in main() loop
        break;
    }
}



/**
 * @brief      Constructs the camshift camera mode.
 *
 * @param      (i.e. openCV)
 */
Camshift::Camshift( VideoCapture * cap) : CameraMode(cap) 
{
	string hot_keys =
    "\n\nHot keys: \n"
    "\tESC - quit the program\n"
    "\tc - stop the tracking\n"
    "\tb - switch to/from backprojection view\n"
    "\th - show/hide object histogram\n"
    "\tp - pause video\n"
    "To initialize tracking, select the object with mouse\n";
	this->setHot_keys(hot_keys);
}


/**
 * @brief      Camshift help callback function.
 */
void Camshift::help()
{
    cout << "\nThis is a demo that shows mean-shift based tracking\n"
            "You select a color objects such as your face and it tracks it.\n"
            "This reads from video camera (0 by default, or the camera number the user enters\n"
            "Usage: \n"
            "   ./camshiftdemo [camera number]\n";
    cout << this->getHot_keys();
}


/**
 * @brief      Camshift camera mode initialization function.
 */
void Camshift::init()
{
	
    if( !this->getCap()->isOpened() )
    {
        cout << "***Could not initialize capturing...***\n";
        return ;
    }

    
    cout << this->getHot_keys();
    
    namedWindow( NOM_FENETRE_PRINCIPALE, 0 );
    resizeWindow(NOM_FENETRE_PRINCIPALE, 800 , 600);
    namedWindow( "Histogram", 0 );
    moveWindow(NOM_FENETRE_PRINCIPALE, 0, 0);
    
    
    setMouseCallback( NOM_FENETRE_PRINCIPALE, onMouse, 0 );
    
    createTrackbar( "Vmin", NOM_FENETRE_PRINCIPALE, &vmin, 256, 0 );
    createTrackbar( "Vmax", NOM_FENETRE_PRINCIPALE, &vmax, 256, 0 );
    createTrackbar( "Smin", NOM_FENETRE_PRINCIPALE, &smin, 256, 0 );

}


/**
 * @brief      Camshift camera mode stop function.
 */
void Camshift::stop()
{
    // destroyWindow(NOM_FENETRE_PRINCIPALE);
    // destroyWindow("Histogram");
}


/**
 * @brief      Camshift camera mode execution process.
 */
void Camshift::exec()
{
	char c='\0';
    
    

    double alpha = 2.0;
    *this->getCap() >> frame;
    if( frame.empty() )
    {
        cerr<<"Camshift::exec() : Warning empty frame detected"<<endl;
        return;
    }
    
    // Applying commons frame corrections for the sake of further
    // process.
    super::frameCorrection(&frame);
    frame.copyTo(image);

    // Extraction of the center of the frame.
    unsigned int centerFrameRows = image.rows / 2;
    unsigned int centerFrameCols = image.cols / 2;
    // cout<<"image.ros = "<<image.rows<< " image.cols " << image.cols<<"centerFrameRow = "<<centerFrameRows <<" , centerFramCols = "<< centerFrameCols<<endl;

    // While there's no detected interruptions
    while(c != 'x')
    {
        if( !paused )
        {
            // Frame information output.
            putText(image, "Camshift mode", cvPoint(30,30),FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0,0,255), 1, CV_AA);
            
            // Transformation of the image structure RGB to HSV.
            cvtColor(image, hsv, COLOR_BGR2HSV);

            if( trackObject )
            {
                int _vmin = vmin, _vmax = vmax;

                inRange(hsv, Scalar(0, smin, MIN(_vmin,_vmax)),
                        Scalar(180, 256, MAX(_vmin, _vmax)), mask);
                int ch[] = {0, 0};
                hue.create(hsv.size(), hsv.depth());
                mixChannels(&hsv, 1, &hue, 1, ch, 1);
  
                if( trackObject < 0 )
                {
                    // Object has been selected by user, set up camShift search properties once
                    Mat roi(hue, selection), maskroi(mask, selection);
                    calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges);
                    normalize(hist, hist, 0, 255, NORM_MINMAX);

                    trackWindow = selection;
                    trackObject = 1; // Don't set up again, unless user selects new ROI 

                    // Constructing selected pixels color apparition probabilities histogram. 
                    histimg = Scalar::all(0);
                    int binW = histimg.cols / hsize;
                    Mat buf(1, hsize, CV_8UC3);
                    for( int i = 0; i < hsize; i++ )
                        buf.at<Vec3b>(i) = Vec3b(saturate_cast<uchar>(i*180./hsize), 255, 255);
                    cvtColor(buf, buf, COLOR_HSV2BGR);

                    for( int i = 0; i < hsize; i++ )
                    {
                        int val = saturate_cast<int>(hist.at<float>(i)*histimg.rows/255);
                        rectangle( histimg, Point(i*binW,histimg.rows),
                                   Point((i+1)*binW,histimg.rows - val),
                                   Scalar(buf.at<Vec3b>(i)), -1, 8 );
                    }
                }

                // Perform CAMShift
                calcBackProject(&hue, 1, 0, hist, backproj, &phranges);
                backproj &= mask;
                RotatedRect trackBox = CamShift(backproj, trackWindow,
                                    TermCriteria( TermCriteria::EPS | TermCriteria::COUNT, 10, 1 ));
                // cout<<"Camshift::exec(): trackBox center ="<<setw(10)<<trackBox.center<<" width = "<<setw(10)<<trackBox.size.width<<" heigth = "<<setw(10)<<trackBox.size.height<<endl;

                // Validating the given tracking box before further analysis
                if(!(trackBox.size.width >= 0 && trackBox.size.height >= 0 && 
                                    trackBox.size.width <= image.cols && trackBox.size.height <= image.rows ))
                {
                    //case when width or height == -nan;
                    //we skip a loop 
                    trackBox.size.width = 0 ; 
                    trackBox.size.height = 0;
                        
                }
                
                if( trackWindow.area() <= 1 )
                {
                    int cols = backproj.cols, rows = backproj.rows, r = (MIN(cols, rows) + 5)/6;
                    trackWindow = Rect(trackWindow.x - r, trackWindow.y - r,
                                       trackWindow.x + r, trackWindow.y + r) &
                                  Rect(0, 0, cols, rows);
                }

                if( backprojMode )
                    cvtColor( backproj, image, COLOR_GRAY2BGR );


                // Drawing ellipse sharing the boundaries and the orientation of the
                // detected tracking box.
                ellipse( image, trackBox, Scalar(0,0,255), 3, LINE_AA );
                
                // Calculating the horizontal and vertical deltas from the center
                // point of the camera generated frame and the new center point of
                // the tracked color given by the ellipse/tracking box.
                int dx = (centerFrameCols - trackBox.center.x);
                int dy = -(centerFrameRows - trackBox.center.y);

                // Drawing the ellipse center
                image.at <char>((trackBox.center.x * image.cols) + trackBox.center.y ) = 255;
                // Drawing the camera generated frame center.
                image.at <char>((centerFrameRows * image.cols) + centerFrameCols  + 1) =  255;

                
                // cout
                // <<"dx = "<<dx <<" "<< (double) dx / (image.cols / 2.0 ) <<endl
                // <<"dy = "<<dy <<" "<< (double) dy / (image.rows / 2.0 ) <<endl;
                // affiche_cadre(image, trackBox.center.x - 1 , trackBox.center.y - 1 , 2 , 2 , 255 , 0 , 0);


                // //======================== servoing

                // The next horizontal and vertical movement determined by the distance between the wanted ptz position given by the center of the ellipse/trackBox) and the current frame center of the camera.
                this->setPanTiltZoom((double) alpha * dx / (image.cols / 2.0 ), (double) alpha *  dy / (image.rows / 2.0 ) , 0 );

                
                // sleep(1);
                

            }



        
        
        }
        else
        {
            this->setPanTiltZoom(0.0,0.0,0);
        }

        if( trackObject < 0 )
            paused = false;
        
        // If mouse selection is valid.
        if( selectObject && selection.width > 0 && selection.height > 0 )
        {   
            // Display rectangular selection in frame
            Mat roi(image, selection);
            bitwise_not(roi, roi);
        }

        // Showing green mini rectangle indicating the center of the frame taken by the PTZ camera.
        affiche_cadre(image, centerFrameCols - 1 , centerFrameRows - 1 , 2 , 2 , 0 , 255 , 0);

        // Show final frame
        imshow( NOM_FENETRE_PRINCIPALE, image );
        // Histogram of the selected pixels color histogram (i.e. Rapport de Travail).
        
        imshow( "Histogram", histimg );

        c = (char)waitKey(30);
        
        // Checking for keyboard events.
        this->checkKeys(c);
        switch(c)
        {
            case 'b':
                // Showing back projection frames (i.e. Rapport de Travail).
                backprojMode = !backprojMode;
                break;
            case 'h':
                // Showing histogram of the selected pixels colors histogram (i.e. Rapport de Travail).
                showHist = !showHist;
                if( !showHist )
                    destroyWindow( "Histogram" );
                else
                    namedWindow( "Histogram", 1 );
                break;
            case 'p':
                // Pause the tracking algorithm
                paused = !paused;
                break;
            case CLAVIER_ECHAP:
                // Exit camshift camera mode and launch standby mode
                this->switchCameraMode(CameraModeFactory::STANDBY);
                c= 'x';
                break;
            
            
         
        }

        // If the tracking algorithm is not paused, the next frame is extracted for tracking purposes.
        if( !paused )
        {

            *this->getCap() >> frame;
            if( frame.empty() )
            {
                cerr<<"Camshift::exec() : Warning empty frame detected"<<endl;
                return;
            }
            super::frameCorrection(&frame);
            frame.copyTo(image);
        }
    }
}



/**
 * @brief      Camera mode keyboard event listener. 
 *
 * @param[in]  key   The keyboard key.
 */
void Camshift::checkKeys(char key)
{
    super::checkKeys(key);

    
    if(key == CLAVIER_ESPACE)
    {
        trackObject = 0;
        histimg = Scalar::all(0);
    }
}