#include "Camera.hpp"
#include "CameraMode.hpp"


/**
 * @brief      Constructs the object without an initial camera mode. The
 *             cameraMode attribute must be set before calling this->execMode().
 */
Camera::Camera()
{
	cameraMode= NULL;
	init();
}


/**
 * @brief      Constructs the object.
 *
 * @param      cameraMode  The camera mode chosen to be executed next if the
 *                         this->execMode() is called.
 */
Camera::Camera(CameraMode * cameraMode)
{
	this->cameraMode = cameraMode;

	
	init();
}


/**
 * @brief      Default real camera position attribute initialization.
 */
void Camera::init() 
{
	this->vpan  = 0;
	this->vtilt = 0;
	this->zoom  = 0;
}


/**
 * @brief      Sets the current camera mode.
 *
 * @param      cameraMode  The camera mode to be next executed.
 */
void Camera::setMode(CameraMode * cameraMode)
{
	this->cameraMode = cameraMode;
}

/**
 * @brief      Gets the current camera mode.
 *
 * @return     The current camera mode.
 */
CameraMode * Camera::getCameraMode()
{
	return cameraMode;
}


/**
 * @brief      Gets the pelcoD camera software controller instance used for
 *             direct communication transfer between the local program and the
 *             camera device.
 *
 * @return     The pelcoD camera software controller instance.
 */
PelcoD* Camera::getPelcoD()
{
	return this->pelcoD;
}


/**
 * @brief      Sets the pelcoD camera software controller instance used for
 *             direct communication transfer between the local program and the
 *             camera device.
 *
 * @param      The pelcoD camera software controller instance.
 */
void Camera::setPelcoD(PelcoD * pelcoD)
{
	this->pelcoD = pelcoD;
}


/**
 * @brief      Enters an infinite loop executing each selected camera mode
 *             sequentially.
 */
void Camera::execMode()
{
	while(1)
	{
		if(cameraMode == NULL)
		{	
			return;
		}
		this->cameraMode->exec();
	}
}


/**
 * @brief      Gets the velocity of the tilt angle.
 *
 * @return     A char value between -0x3f to 0x3f associated to the velocity of
 *             the pan angle.
 */
char Camera::getVpan()
{
	return this->vpan;
}


/**
 * @brief      Gets the velocity of the tilt angle.
 *
 * @return     A char value between -0x3f to 0x3f associated to the velocity of
 *             the tilt angle.
 */
char Camera::getVtilt()
{
	return this->vtilt;
}

/**
 * @brief      Gets the velocity of the pan angle.
 *
 * @return     A double value between -1 to 1 associated to the velocity of
 *             the pan angle.
 */
double Camera::getVpand()
{
	return this->vpan / 63.0;
}

/**
 * @brief      Gets the velocity of the tilt angle.
 *
 * @return     A double value between -1 to 1 associated to the velocity of
 *             the tilt angle.
 */
double Camera::getVtiltd()
{
	return this->vtilt / 63.0;

}


/**
 * @brief      Switch the current camera mode allowing a different behavior of the camera to take control of the camera.
 *
 * @param      cameraMode  The next camera mode to be execute.
 */
void Camera::switchCameraMode(CameraMode * cameraMode)
{
	if(this->getCameraMode() != NULL)
	{
		this->getCameraMode()->stop();
	
	}
	this->setMode(cameraMode);

  if (this->getCameraMode() != NULL) {
    cameraMode->setCamera(this);
    cameraMode->init();
    cameraMode->help();
    cout<<"Camera initialised"<<endl;
  }
}


/**
 @brief      Sends command to the camera device allowing the set the pan tilt
             zoom speeds.

 @param      pand   pan speed double value in between (-1.0 à 1.0).
 @param      tiltd  tilt speed double value in between (-1.0 à 1.0)
 @param      zoom   zoom (at constant speed) to
 				-1: back
 				 1: forward, 
 				 0: no zoom)
*/

void Camera::setPanTiltZoom(double pand , double tiltd , int zoom)
{
	if(pand > 1.0 )
		pand = 1.0;
	else if(pand < -1.0)
		pand = -1.0;

	if(tiltd > 1.0 )
		tiltd = 1.0;
	else if(tiltd < -1.0)
		tiltd = -1.0;

	if(zoom<-1)
		zoom = -1;
	else if(zoom>1)
		zoom =1;



	
	PelcoD* pelcoD = this->getPelcoD();
	char vpan = (char)round(pand * 63.0);
	char vtilt =(char)round(tiltd * 63.0);
	char zoomc = (char) zoom;
	
	if(this->vpan != vpan || this->vtilt != vtilt  || this->zoom !=zoom || (vpan == 0 && vtilt == 0 ))
	{	
		this->vpan  = vpan;
		this->vtilt = vtilt;
		this->zoom  = zoom;
		pelcoD->pan_tilt_zoom(vpan , vtilt , zoomc);
	}
	
}
