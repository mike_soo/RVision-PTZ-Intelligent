#include "StandBy.hpp"


StandBy::StandBy( VideoCapture * cap) : CameraMode(cap)
{
	string hot_keys ="";
    hot_keys+=
    "\n\nHot keys: \n"
    "\tESC   - quit the program\n"
    "\tc     - Visual Servoing - Camshift\n"
    "\to     - Visual Servoing - Optic flow\n"
    "\tp     - Visual Servoing - Pyramidal\n";
    
	// this->setHot_keys(hot_keys);
    super::setHot_keys(hot_keys); 
    
}
 

void StandBy::help()
{
    cout <<"\tWe are watching you\n";
    cout << this->getHot_keys();
}
void StandBy::init()

{
    if( !this->getCap()->isOpened() )
    {
        help();
        cout << "***Could not initialize capturing...***\n";
        return ;
    }
    cout << this->getHot_keys();
    
    namedWindow(NOM_FENETRE_PRINCIPALE,0);
    resizeWindow(NOM_FENETRE_PRINCIPALE,800,600);
    moveWindow(NOM_FENETRE_PRINCIPALE, 0, 0);
}

void StandBy::stop()
{
  cv::destroyAllWindows();
}

void StandBy::exec()
{   

    Mat frame;
    char c='\0';
    

    std::cout << "Current mode: StandBy" << std::endl;

    while(c != 'x')
    {

        *this->getCap() >> frame;
        if( frame.empty() )
            return;


        // flip(frame,frame,0); //Retournement de l'image
        super::frameCorrection(&frame);
        
        Mat frame_d_affichage = frame.clone(); //Image utilisée uniquement pour l'affichage

        affiche_cadre(frame_d_affichage, x_initial, y_initial, mlin, mcol, 255, 0, 0); //Ajout du cadre de ciblage à la frame d'affichage
        putText(frame_d_affichage, "StandBy mode", cvPoint(30,30),FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0,0,255), 1, CV_AA);
        imshow( NOM_FENETRE_PRINCIPALE, frame_d_affichage );


        c = (char)waitKey(10);
        super::checkKeys(c);
        switch(c)
        {
            case 'c':
                cout << "Camshift mode " << endl;
                this->switchCameraMode(CameraModeFactory::CAMSHIFT);
                c = 'x';
                break;

            case 'o':
                cout << "Optical flow mode" << endl;
                this->switchCameraMode(CameraModeFactory::BASICOF);
                c = 'x';
                break;

            case 'p':
                cout << "Pyramidal optical flow" << endl;
                this->switchCameraMode(CameraModeFactory::PYROF);
                c = 'x';
                break;

            case CLAVIER_ECHAP:
              
              this->switchCameraMode(CameraModeFactory::END);
              return;
              break;

            default:
                break;
        }
    }

}

