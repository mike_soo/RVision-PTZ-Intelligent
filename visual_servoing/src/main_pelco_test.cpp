#include "PelcoD.hpp"



#include <new>
#include <iostream>
#include <iomanip>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include "operations_flot_optique.hpp"
#include "gradient_castan.hpp"
#include "PelcoD.hpp"
#include "main.hpp"
#include "Camshift.hpp"
#include "StandBy.hpp"
//Using namespace
using namespace std;
using namespace cv;

int main(){
    //Initialisation de l'input caméra
    VideoCapture cap(1); // On ouvre la caméra selon l'ID (s'il y a une webcam sur l'ordinateur, elle a généralement l'ID 0)
    if(!cap.isOpened()){  // On vérifie que l'ouverture a fonctionné
        cerr << "Impossible d'ouvrir la camera" << endl;
        return -1;
    }else{
        cout << "Camera ouverte" << endl;
    }
    //Initialisation de l'output PTZ
    PelcoD pelcoD(ADRESSE_DIP_CAMERA,(char*)"/dev/ttyUSB0"); // "COMX" pour windows, "/dev/ttyUSBX" pour linux
    //Initialisation de l'affichage
    namedWindow(NOM_FENETRE_PRINCIPALE,WINDOW_AUTOSIZE);
    Camera *camera = new Camera();
	camera->setPelcoD(&pelcoD);
    
    camera->setPanTiltZoom(0.0 , 0.0, 0.0);
    
    double inc = 0.001;
    double commande=0;
    double cpt=1;
    waitKey(0);
    while(1)
    { 
        commande= cpt * inc;
        if(commande > 1)
            break;
        cpt++;
        
        cout<<"commande = "<<commande<<endl;
        camera->setPanTiltZoom(commande , 0.0, 0.0);
    
    }
    waitKey(0);
}
