/**
    @file main_state_machine.cpp
    @brief Programme principal
    @author Michael Ohayon et Yang Chen
    @version 0
    @date 15/04/2018
*/

#include <new>
#include <iostream>
#include <iomanip>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include "operations_flot_optique.hpp"
#include "gradient_castan.hpp"
#include "PelcoD.hpp"
#include "main.hpp"
#include "Camshift.hpp"
#include "StandBy.hpp"
#include <stdlib.h>  
//Using namespace
using namespace std;
using namespace cv;

//Déclaration des variables
int code_clavier = -1; // Code de la touche appuyée au clavier
Mat frame; // Image issue de la caméra
  

int main(int argc ,char ** argv){
    //Initialisation de l'input caméra

    if(argc != 2 )
    {
        cout<<argv[0]<<" idCamera"<<endl;
        exit(-1);
    }

    int idCamera = atoi(argv[1]);
    
    VideoCapture cap(idCamera); // On ouvre la caméra selon l'ID (s'il y a une webcam sur l'ordinateur, elle a généralement l'ID 0)
    if(!cap.isOpened()){  // On vérifie que l'ouverture a fonctionné
        cerr << "Impossible d'ouvrir la camera" << endl;
        return -1;
    }else{
        cout << "Camera ouverte" << endl;
    }
    //Initialisation de l'output PTZ
    PelcoD pelcoD(ADRESSE_DIP_CAMERA,(char*)"/dev/ttyUSB0"); // "COMX" pour windows, "/dev/ttyUSBX" pour linux
    //Initialisation de l'affichage
    namedWindow(NOM_FENETRE_PRINCIPALE,0);
    resizeWindow(NOM_FENETRE_PRINCIPALE,800,600);
    moveWindow(NOM_FENETRE_PRINCIPALE, 0, 0);

    CameraModeFactory::init(&cap);
    // camMode = standBy;
    CameraMode *firstCameraMode = CameraModeFactory::get(CameraModeFactory::STANDBY);
    Camera *camera = new Camera();
    
    if(firstCameraMode == NULL )
        cout<<"main : warning"<<endl;

    firstCameraMode->setCamera(camera);
    camera->switchCameraMode(firstCameraMode);
    camera->setPelcoD(&pelcoD);
    
    camera->execMode();
    return 0;
}
