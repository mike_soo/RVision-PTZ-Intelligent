#include "OpticalFlowPyramidal.hpp"
#include "operations_flot_optique.hpp"
#include "Constants.hpp"
#include "CameraModeFactory.hpp"

OpticalFlowPyramidal::OpticalFlowPyramidal(cv::VideoCapture *cap, unsigned lvl)
  : CameraMode{cap}, quit{false}, mode{Mode::CamControl}, level{lvl + 1}, acc_x{}, acc_y{}, pan_speed{}, tilt_speed{}, lambda{0.5}, text{}, ssm{}
{
  frames = std::vector<cv::Mat>(level);
  gray_frames = std::vector<cv::Mat>(level);
  pyr_frames = std::vector<cv::Mat>(level);
  pyr_motifs = std::vector<cv::Mat>(level);
  pyr_motifs_grad = std::vector<cv::Mat>(level);
  pyr_imagettes = std::vector<cv::Mat>(level);
  pyr_gray_frames = std::vector<cv::Mat>(level);
  pyr_motifs_diff = std::vector<cv::Mat>(level);
  pyr_motif_sizes = std::vector<cv::Size>(level, {0, 0});
  pyr_motif_init_poss = std::vector<cv::Point>(level, {0, 0});
  pyr_motif_rects = std::vector<cv::Rect>(level, {0, 0, 0, 0});
  pyr_motif_mats = std::vector<double*>(level, nullptr);
  pyr_motif_grad_x_mats = std::vector<double*>(level, nullptr);
  pyr_motif_grad_y_mats = std::vector<double*>(level, nullptr);
  pyr_motif_grad_xy_mats = std::vector<double*>(level, nullptr);
  pyr_pseudo_mats = std::vector<double*>(level, nullptr);
  pyr_flot2_mats = std::vector<double*>(level, nullptr);
  pyr_imagette_mats = std::vector<double*>(level, nullptr);
  pyr_motif_diff_mats = std::vector<double*>(level, nullptr);
  pyr_displacement_mats = std::vector<double*>(level, nullptr);
  std::string hot_keys =
    "\n\nHot keys: \n"
    "\tESC   - quitte current mode\n"
    "\tEnter - capture de motif\n"
    "\tSpace - stop suivi\n"
    "\tS     - enregistre les frames\n";
  this->setHot_keys(hot_keys);
}

void OpticalFlowPyramidal::help()
{
  cout << "\tWe are watching you\n";
  cout << this->getHot_keys();
}

void OpticalFlowPyramidal::exec()
{
  std::cout << "Initialization: PyramidalOpticalFlow - level " << level << std::endl;

  while (!quit) {
    this->update_frame();
    this->process_input();

    switch (mode) {
      case Mode::MotifCapture: {
        // for all pyramid levels
        for (unsigned i = 0; i < level; ++i) {
          // extract motif from current pyramid level image
          pyr_motifs[i] = pyr_gray_frames[i](pyr_motif_rects[i]);
          cv::imshow(std::string("PyrMotifGray-lvl") + std::to_string(i), pyr_motifs[i]);

          // convert OpenCV matrix to double array, because the function for calculating gradient takes only basic c++ array.
          mat2double(pyr_motifs[i], pyr_motif_mats[i], pyr_motif_sizes[i].height, pyr_motif_sizes[i].width);
          // calculate motif gradient
          gradient_castan(pyr_motif_mats[i], pyr_motif_grad_x_mats[i], pyr_motif_grad_y_mats[i], pyr_motif_sizes[i].height, pyr_motif_sizes[i].width, 0.4);

          // pseudo inverse matrix for solving over-determined linear system
          std::fill_n(pyr_pseudo_mats[i], 4, 0.0);

          for (int l = 0; l < pyr_motif_sizes[i].height; ++l) {
            for (int c = 0; c < pyr_motif_sizes[i].width; ++c) {
              pyr_pseudo_mats[i][0] += pyr_motif_grad_x_mats[i][l * pyr_motif_sizes[i].width + c] * pyr_motif_grad_x_mats[i][l * pyr_motif_sizes[i].width + c];
              pyr_pseudo_mats[i][1] += pyr_motif_grad_x_mats[i][l * pyr_motif_sizes[i].width + c] * pyr_motif_grad_y_mats[i][l * pyr_motif_sizes[i].width + c];
              pyr_pseudo_mats[i][2] += pyr_motif_grad_x_mats[i][l * pyr_motif_sizes[i].width + c] * pyr_motif_grad_y_mats[i][l * pyr_motif_sizes[i].width + c];
              pyr_pseudo_mats[i][3] += pyr_motif_grad_y_mats[i][l * pyr_motif_sizes[i].width + c] * pyr_motif_grad_y_mats[i][l * pyr_motif_sizes[i].width + c];
            }
          }

          if (pseudo_inverse(pyr_pseudo_mats[i], pyr_pseudo_mats[i], 2, 2) == 0) {
            std::cout << "Impossible d'allouer de la mémoire pour la pseudo inverse" << std::endl;
            return;
          }

          pyr_motifs_grad[i] = pyr_motifs[i].clone();

          for (auto j = 0; j < pyr_motif_sizes[i].area(); ++j)
            pyr_motif_grad_xy_mats[i][j] = std::sqrt(pyr_motif_grad_x_mats[i][j] * pyr_motif_grad_x_mats[i][j] +
                                                     pyr_motif_grad_y_mats[i][j] * pyr_motif_grad_y_mats[i][j]);

          double2mat(pyr_motifs_grad[i], pyr_motif_grad_xy_mats[i], pyr_motif_sizes[i].height, pyr_motif_sizes[i].width);
          pyr_motifs_grad[i] = cv::Scalar::all(255) - pyr_motifs_grad[i];
          cv::imshow(std::string("PyrMotifGradient") + std::to_string(i), pyr_motifs_grad[i]);

          pyr_motif_rects[i] = {pyr_motif_init_poss[i], pyr_motif_sizes[i]};
        }

        pan_speed = 0.0;
        tilt_speed = 0.0;

        acc_x = 0.0;
        acc_y = 0.0;

        text = "Mode Suivi de cible";
        mode = Mode::Tracking;
      } break;

      case Mode::CamControl: {
      } break;

      case Mode::Tracking: {
        std::cout << "==============================" << std::endl;
        for (unsigned i = 0; i < level; ++i) {
          pyr_imagettes[i] = pyr_gray_frames[i](pyr_motif_rects[i]);
          std::fill_n(pyr_displacement_mats[i], 2, 0.0);
          std::fill_n(pyr_flot2_mats[i], 2, 0.0);
          // position on pyramidal images
          extraire_imagette(pyr_gray_frames[i], pyr_imagette_mats[i], pyr_motif_rects[i].x, pyr_motif_rects[i].y, pyr_motif_rects[i].height, pyr_motif_rects[i].width);
          soustraire_matrices(pyr_motif_mats[i], pyr_imagette_mats[i], pyr_motif_diff_mats[i], pyr_motif_sizes[i].height, pyr_motif_sizes[i].width);

          pyr_motifs_diff[i] = pyr_motifs[i].clone();

          double2mat(pyr_motifs_diff[i], pyr_motif_diff_mats[i], pyr_motif_sizes[i].height, pyr_motif_sizes[i].width);
          cv::imshow(std::string("PyrMotifDiff") + std::to_string(i), pyr_motifs_diff[i]);

          // solving over-determined linear system
          pyr_flot2_mats[i][0] = 0; pyr_flot2_mats[i][1] = 0;

          for (auto l = 0; l < pyr_motif_sizes[i].height; ++l) {
            for (auto c = 0; c < pyr_motif_sizes[i].width; ++c) {
              pyr_flot2_mats[i][0] += pyr_motif_grad_x_mats[i][l * pyr_motif_sizes[i].width + c] * pyr_motif_diff_mats[i][l * pyr_motif_sizes[i].width + c];
              pyr_flot2_mats[i][1] += pyr_motif_grad_y_mats[i][l * pyr_motif_sizes[i].width + c] * pyr_motif_diff_mats[i][l * pyr_motif_sizes[i].width + c];
            }
          }

          multiplier_matrices(pyr_pseudo_mats[i], pyr_flot2_mats[i], pyr_displacement_mats[i], 2, 2, 2, 1);
          pyr_motif_rects[i].x += pyr_displacement_mats[i][0];
          pyr_motif_rects[i].y += pyr_displacement_mats[i][1];

          // accumulate displacements of different level of pyramid images
          acc_x += std::pow(2, i) * pyr_displacement_mats[i][0];
          acc_y += std::pow(2, i) * pyr_displacement_mats[i][1];
          std::cout << "displacement: " << std::setprecision(5) << "level " << i << " | x: " << pyr_displacement_mats[i][0] << " y: " << pyr_displacement_mats[i][1] << std::endl;
        }

        std::cout << "displacement: " << std::setprecision(5) << "acc_x: "<< acc_x << " acc_y: "<< acc_y << std::endl;

        std::cout << "difference x= "
                  << acc_x + pyr_motif_rects[0].x - pyr_motif_init_poss[0].x - pyr_displacement_mats[0][0] << " y= "
                  << acc_y + pyr_motif_rects[0].y - pyr_motif_init_poss[0].y - pyr_displacement_mats[0][1] << std::endl;

        pan_speed = (acc_x + pyr_motif_rects[0].x - pyr_motif_init_poss[0].x - pyr_displacement_mats[0][0]) * 2.0 / pyr_frames[0].cols;
        tilt_speed = (acc_y + pyr_motif_rects[0].y - pyr_motif_init_poss[0].y - pyr_displacement_mats[0][1]) * 2.0 / pyr_frames[0].rows;

        acc_x = 0.0;
        acc_y = 0.0;

        std::cout << "pan speed: " << pan_speed << std::endl;
        std::cout << "tilt speed: " <<  tilt_speed << std::endl;

        this->getCamera()->setPanTiltZoom(-pan_speed, tilt_speed, 0);
        std::cout << "==============================" << std::endl;

      } break;
    }
  }

  std::cout << "Exiting OpticalFlowPyramidal ..." << std::endl;
  this->switchCameraMode(CameraModeFactory::STANDBY);
}

void OpticalFlowPyramidal::stop()
{
  for (unsigned i = 0; i < level; ++i) {
    delete [] pyr_motif_diff_mats[i];
    pyr_motif_diff_mats[i] = nullptr;
    delete [] pyr_displacement_mats[i];
    pyr_displacement_mats[i] = nullptr;
    delete [] pyr_imagette_mats[i];
    pyr_imagette_mats[i] = nullptr;
    delete [] pyr_flot2_mats[i];
    pyr_flot2_mats[i] = nullptr;
    delete [] pyr_pseudo_mats[i];
    pyr_pseudo_mats[i] = nullptr;
    delete [] pyr_motif_mats[i];
    pyr_motif_mats[i] = nullptr;
    delete [] pyr_motif_grad_x_mats[i];
    pyr_motif_grad_x_mats[i] = nullptr;
    delete [] pyr_motif_grad_y_mats[i];
    pyr_motif_grad_y_mats[i] = nullptr;
    delete [] pyr_motif_grad_xy_mats[i];
    pyr_motif_grad_xy_mats[i] = nullptr;
  }

  cv::destroyAllWindows();
}

void OpticalFlowPyramidal::init()
{
  if (!this->getCap()->isOpened()) {
    
    cout << "***Could not initialize capturing.***\n";
    return ;
  }
  std::cout << this->getHot_keys();

  this->getCap()->read(pyr_frames[0]);
  cv::flip(pyr_frames[0], pyr_frames[0], 0);

  for (unsigned i = 1; i < pyr_frames.size(); ++i) {
    cv::pyrDown(pyr_frames[i-1], pyr_frames[i], {pyr_frames[i-1].cols / 2, pyr_frames[i-1].rows / 2});
  }

  for (unsigned i = 0; i < pyr_frames.size(); ++i) {
    cv::cvtColor(pyr_frames[i], pyr_gray_frames[i], cv::COLOR_BGR2GRAY);
    pyr_motif_sizes[i] = {pyr_frames[i].cols / 3, pyr_frames[i].rows / 3};
    // init position on pyramidal images
    pyr_motif_init_poss[i] = {(pyr_frames[i].cols - pyr_motif_sizes[i].width) / 2,
                              (pyr_frames[i].rows - pyr_motif_sizes[i].height) / 2};
    pyr_motif_rects[i] = {pyr_motif_init_poss[i], pyr_motif_sizes[i]};
  }

  try {
    for (unsigned i = 0; i < pyr_frames.size(); ++i) {
      pyr_motif_mats[i] = new double[pyr_motif_sizes[i].area()];
      pyr_motif_grad_x_mats[i] = new double[pyr_motif_sizes[i].area()];
      pyr_motif_grad_y_mats[i] = new double[pyr_motif_sizes[i].area()];
      pyr_motif_grad_xy_mats[i] = new double[pyr_motif_sizes[i].area()];
      pyr_pseudo_mats[i] = new double[2 * 2];
      pyr_flot2_mats[i] = new double[2 * 1];
      pyr_imagette_mats[i] = new double[pyr_motif_sizes[i].area()];
      pyr_motif_diff_mats[i] = new double[pyr_motif_sizes[i].area()];
      pyr_displacement_mats[i] = new double[2];

      std::fill_n(pyr_displacement_mats[i], 2, 0.0);
      std::fill_n(pyr_pseudo_mats[i], 4, 0.0);
      std::fill_n(pyr_flot2_mats[i], 2, 0.0);
    }

  } catch (std::bad_alloc&) {
    std::cout << "Memory allocation eprror! Exit..." << std::endl;
    return;
  }

  this->mode = Mode::CamControl;

  quit = false;

}

void OpticalFlowPyramidal::process_input()
{
  int key = cv::waitKey(25);

  if (mode == Mode::CamControl)
    this->checkKeys(key);

  switch (key) {
    case CLAVIER_ECHAP: {
      std::cout << "ESC" << std::endl;
      quit = true;
    } break;

    case CLAVIER_ESPACE: {
      for (unsigned i = 0; i < level; ++i)
        pyr_motif_rects[i] = {pyr_motif_init_poss[i], pyr_motif_sizes[i]};

      if (mode == Mode::CamControl) {
        this->getCamera()->getPelcoD()->retour_position_initiale();
      }

      if (mode == Mode::Tracking) {
        for (unsigned i = 0; i < pyr_motif_rects.size(); ++i) {
          pyr_motif_rects[i] = {pyr_motif_init_poss[i], pyr_motif_sizes[i]};
        }
        mode = Mode::CamControl;
        text = "Mode camera control";
      }

    } break;

    case CLAVIER_ENTREE: {

      if (mode == Mode::CamControl) {
        std::cout << "CamControl: Prise d'un nouveau motif" << std::endl;
        text = "Nouveau motif";
        mode = Mode::MotifCapture;
      }

     if (mode == Mode::Tracking) {
       for (unsigned i = 0; i < level; ++i)
         pyr_motif_rects[i] = {pyr_motif_init_poss[i], pyr_motif_sizes[i]};
        std::cout << "Tracking: Prise d'un nouveau motif" << std::endl;
        text = "Nouveau motif";
        mode = Mode::MotifCapture;
      }
    } break;

    case CLAVIER_S: {
      std::cout << "Saveing images" << std::endl;
      for (unsigned i = 0; i < level; ++i) {
        ssm.str("");
        ssm << "./imgs/frames_" << i << ".jpeg";
        std::cout << ssm.str() << std::endl;
        cv::imwrite(ssm.str(), frames[i]);
        ssm.str("");

        ssm << "./imgs/pyr_frames_" << i << ".jpeg";
        cv::imwrite(ssm.str(), pyr_frames[i]);
        std::cout << ssm.str() << std::endl;
        ssm.str("");

        ssm << "./imgs/pyr_gray_frames_" << i << ".jpeg";
        cv::imwrite(ssm.str(), pyr_gray_frames[i]);
        std::cout << ssm.str() << std::endl;
        ssm.str("");

        ssm << "./imgs/pyr_motifs_grad_" << i << ".jpeg";
        cv::imwrite(ssm.str(), pyr_motifs_grad[i]);
        std::cout << ssm.str() << std::endl;
        ssm.str("");

        ssm << "./imgs/pyr_motifs_diff_" << i << ".jpeg";
        cv::imwrite(ssm.str(), pyr_motifs_diff[i]);
        std::cout << ssm.str() << std::endl;
        ssm.str("");

        ssm << "./imgs/pyr_imagettes_" << i << ".jpeg";
        cv::imwrite(ssm.str(), pyr_imagettes[i]);
        std::cout << ssm.str() << std::endl;
        ssm.str("");
      }
    } break;

    default: {
      break;
    }
  }
}

OpticalFlowPyramidal::~OpticalFlowPyramidal()
{
  this->stop();
}

void OpticalFlowPyramidal::update_frame()
{
  Mat frame;
  this->getCap()->read(pyr_frames[0]);

  this->frameCorrection(&pyr_frames[0]);

  frames[0] = pyr_frames[0].clone();

  for (unsigned i = 1; i < pyr_frames.size(); ++i) {
    cv::pyrDown(pyr_frames[i-1], pyr_frames[i], {pyr_frames[i-1].cols / 2, pyr_frames[i-1].rows / 2});
    frames[i] = pyr_frames[i].clone();
  }

  // draw motif rectangle to screen
  for (unsigned i = 0; i < pyr_frames.size(); ++i) {

    cv::rectangle(frames[i], pyr_motif_rects[i], {(double)(i + 1) * (255 / level), (double)(i + 1) * (60 / level), (double)(i + 1) * (100 / level)}, 2, cv::LINE_4, 0);

    cv::Rect pos = {cv::Point{pyr_motif_rects[i].x + (pyr_frames[0].cols - pyr_frames[i].cols) / 2,
                              pyr_motif_rects[i].y + (pyr_frames[0].rows - pyr_frames[i].rows) / 2},
                    cv::Size{pyr_motif_rects[i].width, pyr_motif_rects[i].height}};

    cv::rectangle(frames[0], pos, {(double)(i + 1) * (255 / level), (double)(i + 1) * (600 / level), (double)(i + 1) * (100 / level)}, 2, cv::LINE_4, 0);
    cv::rectangle(gray_frames[0], pos, {(double)(i + 1) * (255 / level), (double)(i + 1) * (60 / level), (double)(i + 1) * (100 / level)}, 2, cv::LINE_4, 0);

    cv::cvtColor(pyr_frames[i], pyr_gray_frames[i], cv::COLOR_BGR2GRAY);
    gray_frames[i] = pyr_gray_frames[i].clone();

    cv::imshow(std::string("Camera PTZ Hikvision-Frame-lv") + std::to_string(i), frames[i]);
    cv::imshow(std::string("Camera PTZ Hikvision-Frame-gray-lv") + std::to_string(i), gray_frames[i]);
  }

  cv::imshow("Camera PTZ Hikvision", frames[0]);
}
