#include "CameraMode.hpp"




/**
 * @brief      Constructs the object.
 *
 * @param      cap   A VideoCapture instance (i.e. opencv) allowing to get a
 *                   flow of frame from the device camera. This parameter must
 *                   already be linked to a camera. See the opencv
 *                   documentations to get more details of this procedure.
 */
CameraMode::CameraMode(VideoCapture *cap)
{
	this->cap = cap;
}


/**
 * @brief      Constructs the object.
 *
 * @param      cap     A VideoCapture instance (i.e. opencv) allowing to get a
 *                     flow of frame from the device camera. This parameter must
 *                     already be linked to a camera. See the opencv
 *                     documentations to get more details of this procedure.
 * @param      camera  An already configured Camera instance. (i.e. Camera class). 
 */
CameraMode::CameraMode(VideoCapture *cap , Camera * camera)
{
	this->cap = cap;
	this->camera = camera;
}


/**
 * @brief      Sets the associated camera.
 *
 * @param      camera  The associated camera instance.
 */
void CameraMode::setCamera(Camera* camera)
{
	this->camera= camera;
}
Camera *CameraMode::getCamera()
{
	return this->camera;
}
VideoCapture * CameraMode::getCap()
{
	return this->cap;
}

string CameraMode::getHot_keys()
{
	return hot_keys;	
} 
void CameraMode::setHot_keys(const string &hot_keys)
{
	this->hot_keys = hot_keys;
	
} 

void CameraMode::switchCameraMode(CameraModeFactory::CameraModes mode)
{
	this->camera->switchCameraMode(CameraModeFactory::get(mode));
}

void CameraMode::setPanTiltZoom(double pan , double tilt , int zoom)
{
	
	this->getCamera()->setPanTiltZoom(pan,tilt,zoom);
	
}
	


void CameraMode::checkKeys(char key)
{
	checkArrowKeys(key);
	checkZoom(key);
}

void CameraMode::checkArrowKeys(char key)
{
	// cout<<"key ="<<(int)key<<endl;

	if(key == CLAVIER_HAUT || key == 56)
	{
		camera->setPanTiltZoom( 0.0 , -0.8 , 0.0);
		// cout<<"CameraMode::checkArrowKeys() : HAUT()"<<endl;
	}
	else if(key == CLAVIER_BAS || key == 53)
	{
		camera->setPanTiltZoom( 0.0 ,  0.8 , 0.0);
		// cout<<"CameraMode::checkArrowKeys() : _BAS()"<<endl;
	}
	else if(key == CLAVIER_DROITE || key == 54)
	{
		camera->setPanTiltZoom(-0.8 ,  0.0 , 0.0);
		// cout<<"CameraMode::checkArrowKeys() : DROITE()"<<endl;
	}
	else if(key == CLAVIER_GAUCHE || key == 52)
	{
		camera->setPanTiltZoom( 0.8 ,  0.0 , 0.0);	
		// cout<<"CameraMode::checkArrowKeys() : GAUCHE()"<<endl;
	}
	else if(key == CLAVIER_ESPACE )
	{
		camera->setPanTiltZoom( 0.0 ,  0.0 , 0.0);
		// cout<<"CameraMode::checkArrowKeys() : ESPACE()"<<endl;
	}
}

void CameraMode::checkZoom(char key)
{
	if(key== ZOOM_IN)
	{
		camera->setPanTiltZoom(camera->getVpand(), camera->getVtiltd(), 1);
	}
	else if (key == ZOOM_OUT)
	{
		camera->setPanTiltZoom(camera->getVpand(), camera->getVtiltd(), -1);	
	}
}

// TODO : all operations of opensCV seems to be pass by copy (whole structure)
// there should be a way to pass by pointer 
void CameraMode::frameCorrection(Mat *frame)
{
	flip(*frame,*frame,-1);
	// return ImageOperations::rotate(frame, 180);

}
