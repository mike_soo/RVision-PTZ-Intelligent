/**
    @file operations_flot_optique.cpp
    @brief Diverses fonctions d'op�rations sur les matrices n�cessaire au flot optique
    @author Tangi Pointeau et Albane Ordrenneau
    @version 2.1
    @date 15/02/2018
*/

#include "operations_flot_optique.hpp"

/**
    Copie des valeurs de pixels d'une image openCV Mat dans un tableau de doubles

    @param image Image de type Mat � copier
    @param *tableau Pointeur vers le tableau de doubles de taille lin*col o� copier les pixels
    @param lin Nombre de lignes dans l'image � copier
    @param col Nombre de colonnes dans l'image � copier
*/
void mat2double(cv::Mat image, double *tableau, int lin, int col){
    for(int i=0; i<lin; i++){
        for(int j=0; j<col;j++){
                tableau[i*col+j] = image.data[image.step[0]*i + image.step[1]*j];
        }
    }
}


/**
    Copie des valeurs d'un tableau de doubles dans les pixels d'une image openCV Mat

    @param image Image de type Mat dans laquelle copier les valeurs
    @param *tableau Pointeur vers le tableau de doubles de taille lin*col dont on copie les valeurs
    @param lin Nombre de lignes dans l'image dans laquelle copier
    @param col Nombre de colonnes dans l'image dans laquelle copier
*/
void double2mat(cv::Mat image, double *tableau, int lin, int col){
    for(int i = 0; i < lin; i++){
        for(int j = 0; j < col; j++){
            image.data[image.step[0]*i + image.step[1]*j+2] = tableau[i*col+j];
            image.data[image.step[0]*i + image.step[1]*j+1] = tableau[i*col+j];
            image.data[image.step[0]*i + image.step[1]*j]   = tableau[i*col+j];
        }
    }
}





/**
    Ajoute un cadre rouge sur l'image en param�tre

    @param image Image au format Mat sur laquelle rajouter le cadre
    @param x Coordonn�e de la colonne du coin sup�rieur-gauche du cadre
    @param y Coordonn�e ligne du coin sup�rieur-gauche du cadre
    @param lin Taille verticale du cadre
    @param col Taille horizontale du cadre
    @param rouge Quantit� de rouge dans la couleur du cadre (0 � 255)
    @param vert Quantit� de vert dans la couleur du cadre (0 � 255)
    @param bleu Quantit� de bleu dans la couleur du cadre (0 � 255)
*/
// void affiche_cadre(cv::Mat image, int x, int y, int lin, int col, int rouge, int vert, int bleu){
//     //Lignes horizontales
//     for(int i=0; i<col; i++){
//         //Ligne du haut
//         image.data[image.step[0]*y + image.step[1]*(x+i)+2] = rouge;
//         image.data[image.step[0]*y + image.step[1]*(x+i)+1] = vert;
//         image.data[image.step[0]*y + image.step[1]*(x+i)]   = bleu;
//         //Ligne du bas
//         image.data[image.step[0]*(y+lin) + image.step[1]*(x+i)+2] = rouge;
//         image.data[image.step[0]*(y+lin) + image.step[1]*(x+i)+1] = vert;
//         image.data[image.step[0]*(y+lin) + image.step[1]*(x+i)]   = bleu;
//     }
//     //Lignes verticales
//     for(int i=0; i<lin; i++){
//         //Ligne de gauche
//         image.data[image.step[0]*(y+i) + image.step[1]*x+2] = rouge;
//         image.data[image.step[0]*(y+i) + image.step[1]*x+1] = vert;
//         image.data[image.step[0]*(y+i) + image.step[1]*x]   = bleu;
//         //Ligne de droite
//         image.data[image.step[0]*(y+i) + image.step[1]*(x+col)+2] = rouge;
//         image.data[image.step[0]*(y+i) + image.step[1]*(x+col)+1] = vert;
//         image.data[image.step[0]*(y+i) + image.step[1]*(x+col)]   = bleu;
//     }
// }


/**
    Ajoute un cadre rouge sur l'image en param�tre

    @param image Image au format Mat sur laquelle rajouter le cadre
    @param x Coordonn�e de la colonne du coin sup�rieur-gauche du cadre
    @param y Coordonn�e ligne du coin sup�rieur-gauche du cadre
    @param lin Taille verticale du cadre
    @param col Taille horizontale du cadre
    @param rouge Quantit� de rouge dans la couleur du cadre (0 � 255)
    @param vert Quantit� de vert dans la couleur du cadre (0 � 255)
    @param bleu Quantit� de bleu dans la couleur du cadre (0 � 255)
*/
void affiche_cadre(cv::Mat &image, int x, int y, int lin, int col, int rouge, int vert, int bleu){
    //Lignes horizontales
    for(int i=0; i<col; i++){
        //Ligne du haut
        image.data[image.step[0]*y + image.step[1]*(x+i)+2] = rouge;
        image.data[image.step[0]*y + image.step[1]*(x+i)+1] = vert;
        image.data[image.step[0]*y + image.step[1]*(x+i)]   = bleu;
        //Ligne du bas
        image.data[image.step[0]*(y+lin) + image.step[1]*(x+i)+2] = rouge;
        image.data[image.step[0]*(y+lin) + image.step[1]*(x+i)+1] = vert;
        image.data[image.step[0]*(y+lin) + image.step[1]*(x+i)]   = bleu;
    }
    //Lignes verticales
    for(int i=0; i<lin; i++){
        //Ligne de gauche
        image.data[image.step[0]*(y+i) + image.step[1]*x+2] = rouge;
        image.data[image.step[0]*(y+i) + image.step[1]*x+1] = vert;
        image.data[image.step[0]*(y+i) + image.step[1]*x]   = bleu;
        //Ligne de droite
        image.data[image.step[0]*(y+i) + image.step[1]*(x+col)+2] = rouge;
        image.data[image.step[0]*(y+i) + image.step[1]*(x+col)+1] = vert;
        image.data[image.step[0]*(y+i) + image.step[1]*(x+col)]   = bleu;
    }
}

/**
    Copie des valeurs de pixels d'une zone d'une image openCV Mat dans un tableau de doubles

    @param image Image de type Mat dans laquelle on copie une zone
    @param *tableau Pointeur vers le tableau de doubles de taille lin*col o� copier les pixels
    @param x Coordonn�e horizontale du coin sup�rieur gauche de la zone � copier
    @param y Coordonn�e verticale du coin sup�rieur gauche de la zone � copier
    @param lin Nombre de lignes dans la zone � copier
    @param col Nombre de colonnes dans la zone � copier
*/
void extraire_imagette(cv::Mat image, double *tableau, int x, int y, int lin, int col){
    for(int i=0; i<lin; i++){
        for(int j=0; j<col;j++){
                tableau[i*col+j] = image.data[image.step[0]*(i+y) + image.step[1]*(j+x)];
        }
    }
}




/**
    Remplissage d'une matrice identit�

    @param *matrice_in Pointeur vers le tableau de doubles � remplir de taille dim*dim
    @param dim Dimension de la matrice � remplir
*/
void remplir_identite(double *tableau, int dim){
    for(int i=0; i<dim; i++){
        for(int j=0; j<dim; j++){
            if(i==j){
                tableau[i+j*dim] = 1;
            }else{
                tableau[i+j*dim] = 0.;
            }
        }
    }
}


/**
    Cette fonction permet de recopier le contenu d'une matrice dans une autre

    @param *matrice_source Pointeur vers la matrice � recopier de taille lin*col
    @param *matrice_cible Pointeur vers la matrice dans laquelle recopier de taille lin*col
    @param lin Nombre de lignes de la matrice � copier
    @param col Nombre de colonnes de la matrice � copier
*/
void copier_matrice(double *matrice_source, double *matrice_cible, int lin, int col){
    for(int i=0; i<lin; i++){
        for(int j=0; j<col; j++){
            matrice_cible[i*col+j] = matrice_source[i*col+j];
        }
    }
}


/**
    Cette fonction permet de transposer une matrice

    @param *matrice_in Pointeur vers la matrice � transposer de taille lin*col
    @param *matrice_out Pointeur vers la matrice transpos�e de taille lin*col
    @param lin Nombre de lignes dans la matrice � transposer
    @param col Nombre de colonnes dans la matrice � transposer
*/
void transposer_matrice(double *matrice_in, double *matrice_out, int lin,int col){
    for(int i=0; i<lin; i++){
        for(int j=0; j<col; j++){
            matrice_out[j*lin+i] = matrice_in[i*col+j];
        }
    }
}


/**
    Cette fonction permet de faire le produit de deux matrices
    Si les dimensions des deux matrices de correspondent pas, la matrice de sortie sera mise � 0

    @param *matrice_in_gauche Pointeur vers la matrice � gauche du produit de taille lin1*col1
    @param *matrice_in_droite Pointeur vers la matrice � droite du produit de taille lin2*col2
    @param *matrice_out Pointeur vers la matrice o� stocker le r�sultat du produit de taille col1*lin2
    @param lin1 Nombre de lignes dans la matrice � gauche du produit
    @param col1 Nombre de colonnes dans la matrice � gauche du produit
    @param lin2 Nombre de lignes dans la matrice � droite du produit
    @param col2 Nombre de colonnes dans la matrice � droite du produit
*/
void multiplier_matrices(double *matrice_in_gauche, double *matrice_in_droite, double *matrice_out, int lin1, int col1, int lin2, int col2){
    // Initialisation de la matrice de sortie
    for(int i = 0; i < lin1; ++i){
        for(int j = 0; j < col2; ++j){
            matrice_out[i*col2+j] = 0.;
        }
    }
    //V�rification de la taille des matrices
    if(col1!=lin2){
        std::cout << "Produit impossible, les dimensions sont incorrectes" << std::endl;
        std::cout << "Dimension matrice 1 : " << lin1 << "*" << col1 << std::endl;
        std::cout << "Dimension matrice 2 : " << lin2 << "*" << col2 << std::endl;
        std::cout << "ncol1 doit �tre �gal � nlin2" << std::endl;
    }else{
        //Multiplication des matrices
        for(int i = 0; i < lin1; ++i){
            for(int j = 0; j < col2; ++j){
                for(int k=0; k<col1; ++k){
                    matrice_out[i*col2+j] += matrice_in_gauche[i*col1+k] * matrice_in_droite[k*col2+j];
                }
            }
        }
    }
}



/**
    Cette fonction permet d'inverser une matrice carr�e de d�terminant non nul

    @param *matrice_in Pointeur vers la matrice � inverser  de taille dim*dim
    @param *matrice_out Pointeur vers la matrice inverse de taille dim*dim
    @param dim Dimension de la matrice carr�e
*/
//la matrice mat1 est celle qui est � inverser
//et l'inverse est contenu dans mat2
void inverser_matrice(double *matrice_in, double *matrice_out, int dim){
    double *temp = new double[dim*dim]; //Matrice locale pour �viter d'�craser celle en entr�e
    copier_matrice(matrice_in, temp, dim, dim);
    double a = 0.;
    double b = 0.;
    int c = 0;
    remplir_identite(matrice_out, dim);
    for(int k=0; k<dim; k++){
        a = temp[k+k*dim];
        c = 0;
        while(abs(a) < 0.000000001){
            c++;
            for(int q=0; q<dim; q++){
                temp[k+q*dim] = temp[k+q*dim] + temp[k+c+q*dim];
                matrice_out[k+q*dim] = matrice_out[k+q*dim] + matrice_out[k+c+q*dim];
            }
            a = temp[k+k*dim];
        }
        //normalisation de la ligne k
        for(int l=0; l<dim; l++){
            temp[k+l*dim] = temp[k+l*dim]/a;
            matrice_out[k+l*dim] = matrice_out[k+l*dim]/a;
        }
        //reduction de gauss-jordan
        for(int i=0; i<dim; i++){
            b = temp[i+k*dim];
            if(i != k){
                for(int j=0; j<dim; j++){
                    temp[i+j*dim] = temp[i+j*dim] - b*temp[k+j*dim];
                    matrice_out[i+j*dim] = matrice_out[i+j*dim] - b*matrice_out[k+j*dim];
                }
            }
        }
    }
    delete[] temp;
}



/**
    Cette fonction permet d'inverser une matrice en calculant la pseudo-inverse de Penrose-Moore
    Telle que pinv(A) = inv(trans(A)*A)*trans(A)

    @param *matrice_in Pointeur vers la matrice � inverser de taille lin*col
    @param *matrice_out Pointeur vers la matrice pseudo-inverse de taille col*lin
    @param lin Nombre de lignes dans la matrice � inverser
    @param col Nombre de colonnes dans la matrice � inverser
*/

int pseudo_inverse(double *matrice_in,double *matrice_out,int lin, int col){
    double *ATA,*AT;
    int i,j,k;
    double *pt,*pt1,*pt2;

    try {
      ATA = new double [col*col];
      AT = new double [lin*col];
    } catch (std::bad_alloc&) {
      std::cout << "Memory allocation eprror! Exit..." << std::endl;
      return 0;
    }

    std::cout << "DEBUG: pseudo_inverse 0" << std::endl;
    pt=ATA;

    for (i=0;i<col;i++){
        for(j=0;j<col;j++,pt++){
            pt1=matrice_in+i;
            pt2=matrice_in+j;
            (*pt)=0.0;
            for (k=0;k<lin;k++){
                (*pt)+=(*pt1)*(*pt2);
                pt1+=col;
                pt2+=col;
            }
        }
    }

    std::cout << "DEBUG: pseudo_inverse 1" << std::endl;

    pt1=matrice_in;
    for (i=0;i<lin;i++){
        pt2=AT+i;
        for(j=0;j<col;j++){
            (*pt2)=(*pt1);
            pt1++;
            pt2+=lin;
        }
    }

    std::cout << "DEBUG: pseudo_inverse 2" << std::endl;

    inverser_matrice(ATA,ATA,col);
    std::cout << "DEBUG: pseudo_inverse 3" << std::endl;
    transposer_matrice(matrice_in,AT,lin,col);
    std::cout << "DEBUG: pseudo_inverse 4" << std::endl;
    multiplier_matrices(ATA,AT,matrice_out,col,col,col,lin);
    delete [] ATA;
    delete [] AT;
    std::cout << "DEBUG: end pseudo_inverse" << std::endl;
    return 1;
}



/**
    Effectue la diff�rence de deux matrices

    @param *matrice_in_gauche Pointeur vers la matrice � gauche de la soustraction de taille lin*col
    @param *matrice_in_droite Pointeur vers la matrice � droite de la soustraction de taille lin*col
    @param *matrice_out Pointeur vers la matrice o� stocker le r�sultat de la soustraction de taille lin*col
    @param lin Nombre de lignes dans les matrices de la soustraction
    @param col Nombre de colonnes dans les matrices de la soustraction
*/
void soustraire_matrices(double *matrice_in_gauche, double *matrice_in_droite, double *matrice_out, int lin, int col){
    // Initialisation de la matrice de sortie
    for(int i = 0; i < lin; ++i){
        for(int j = 0; j < col; ++j){
            matrice_out[i*col+j] = 0.;
        }
    }
    //Soustraction des matrices
    for(int i = 0; i < lin; ++i){
        for(int j = 0; j < col; ++j){
            matrice_out[i*col+j] = matrice_in_gauche[i*col+j] - matrice_in_droite[i*col+j];
        }
    }
}





