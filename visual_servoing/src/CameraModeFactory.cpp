#include "CameraModeFactory.hpp"
#include "OpticalFlowPyramidal.hpp"
#include "Camshift.hpp"
#include "StandBy.hpp"
#include "Camera.hpp"


map<CameraModeFactory::CameraModes,CameraMode*> * CameraModeFactory::cameraModeInstances = new map<CameraModeFactory::CameraModes,CameraMode* >();


/**
 * @brief      Gets if exists an unique instance of the requested camera mode.
 *
 * @param[in]  mode  The desired camera mode
 *
 * @return
 */
CameraMode * CameraModeFactory::get(CameraModeFactory::CameraModes mode)
{
	//verify if element is in map
	if(mode == CameraModeFactory::END)
		return nullptr;

	map<CameraModeFactory::CameraModes , CameraMode *>::iterator it = CameraModeFactory::cameraModeInstances->find(mode);

	if(it!= CameraModeFactory::cameraModeInstances->end())
		return it->second;

	else
		return nullptr;

}


/**
 *
 * @brief
 *
 * @param cap
 */
void CameraModeFactory::init(VideoCapture *cap)
{
	//Initialisation of standby instance
	CameraModeFactory::cameraModeInstances->insert((make_pair(CameraModeFactory::STANDBY , new StandBy(cap))));
	//Initialisation of camshift instance
	CameraModeFactory::cameraModeInstances->insert((make_pair(CameraModeFactory::CAMSHIFT , new Camshift(cap))));
  // Initialisation of basic optical flow install
  CameraModeFactory::cameraModeInstances->insert((make_pair(CameraModeFactory::BASICOF, new OpticalFlowPyramidal(cap, 0))));
  // Initialisation of pyramidal optical flow
  CameraModeFactory::cameraModeInstances->insert((make_pair(CameraModeFactory::PYROF, new OpticalFlowPyramidal(cap, 3))));
}
