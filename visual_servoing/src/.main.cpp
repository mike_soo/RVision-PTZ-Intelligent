/**
    @file main.cpp
    @brief Programme principal
    @author Tangi Pointeau et Albane Ordrenneau
    @version 3
    @date 15/02/2018
*/

#include <new>
#include <iostream>
#include <iomanip>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include "operations_flot_optique.hpp"
#include "gradient_castan.hpp"
#include "pelco_d.hpp"
#include "main.hpp"
#include "Camshift.hpp"
#include "StandBy.hpp"
//Using namespace
using namespace std;
using namespace cv;

//D�claration des variables
int code_clavier = -1; // Code de la touche appuy�e au clavier
Mat frame; // Image issue de la cam�ra
Mat motif; // Motif a suivre
Mat gradient; // Image pour afficher le gradient
int mode = 0; // Variable de s�lection du mode (0 = contr�le clavier, 1 = s�lection d'un nouveau motif, 2 = suivi de cible)
double y_courant = y_initial; // position verticale du coin sup�rieur gauche du cadre de de suivi
double x_courant = x_initial; // position horizontale du coin sup�rieur gauche du cadre de de suivi
double vitesse_pan = 0; // Vitesse de pan de la cam�ra (-0x3f � 0x3f)
double vitesse_tilt = 0; // Vitesse de tilt de la cam�ra (-0x3f � 0x3f)
char sens_zoom = 0; // Sens de zoom de la cam�ra (-1 zoom arri�re, 1 zoom avant, 0 pas de zoom)
string texte_ecran = "Mode controle clavier"; // Texte qui s'affiche sur la fen�tre principale
double lambda = 0.5; // Facteur de vitesse de l'asservissement 
double accumulateur_x = 0; // Accumulateur contenant la somme des d�placements horizontaux
double accumulateur_y = 0; // Accumulateur contenant la somme des d�placements verticaux 

//D�claration des pointeurs de tableaux
double *matrice_motif; // Matrice de taille mlin*mcol contenant le motif � suivre 
double *matrice_pseudo; // Matrice de taille 2*2 du calcul du flot optique (� pseudo-inverser) 
double *matrice_flot2; // Matrice 2*1 du calcul du flot optique */
double *matrice_imagette_courante; // Matrice de taille mlin*mcol contenant l'imagette courante � comparer au motif 
double *matrice_difference; // Matrice de taille mlin*mcol contenant la diff�rence entre le motif et l'imagette courante 
double *vecteur_deplacement; // Matrice 2*1 contenant les d�placements calcul�s dx et dy du motif 
double *gradient_x; // Matrice de taille mlin*mcol contenant le gradient en x du motif 
double *gradient_y; // Matrice de taille mlin*mcol contenant le gradient en y du motif 
double *gradient_xy; // Matrice de taille mlin*mcol contenant le gradient xy du motif. Elle ne sert qu� l'affichage 



CameraMode *camMode;

int main(){
    //Initialisation de l'input cam�ra
    VideoCapture cap(0); // On ouvre la cam�ra selon l'ID (s'il y a une webcam sur l'ordinateur, elle a g�n�ralement l'ID 0)
    if(!cap.isOpened()){  // On v�rifie que l'ouverture a fonctionn�
        cerr << "Impossible d'ouvrir la camera" << endl;
        return -1;
    }else{
        cout << "Camera ouverte" << endl;
    }
    //Initialisation de l'output PTZ
    PelcoD pelco(ADRESSE_DIP_CAMERA,(char*)"/dev/ttyUSB0"); // "COMX" pour windows, "/dev/ttyUSBX" pour linux
    //Initialisation de l'affichage
    namedWindow(NOM_FENETRE_PRINCIPALE,WINDOW_AUTOSIZE);
    namedWindow(NOM_FENETRE_MOTIF,WINDOW_AUTOSIZE);
    namedWindow("Gradient",WINDOW_AUTOSIZE);
    //Prise d'une premi�re image pour connaitre la taille
    cap >> frame; //R�cup�ration de l'image courante
    flip(frame,frame,0); //Retournement de l'image
    Mat frame_d_affichage = frame.clone(); //Image utilis�e uniquement pour l'affichage
    cvtColor(frame,frame,COLOR_BGR2GRAY); // Mise en nuances de gris
    //Allocation dynamique des diff�rentes matrices de doubles utilis�es
    double *matrice_motif = new double[mlin*mcol];
    double *matrice_pseudo = new double[2*2];
    double *matrice_flot2 = new double[2*1];
    double *matrice_imagette_courante = new double[mlin*mcol];
    double *matrice_difference = new double[mlin*mcol];
    double *vecteur_deplacement = new double[2];
    double *gradient_x = new double[mlin*mcol];
    double *gradient_y = new double[mlin*mcol];
    double *gradient_xy = new double[mlin*mcol];
    //V�rification de l'allocation de la m�moire
    if ((matrice_motif==nullptr)||(matrice_pseudo==nullptr)||(matrice_imagette_courante==nullptr)||(matrice_difference==nullptr)
        ||(vecteur_deplacement==nullptr)||(gradient_x==nullptr)||(gradient_y==nullptr)||(gradient_x==nullptr)||(matrice_flot2==nullptr)){
        delete[] matrice_motif;
        delete[] matrice_pseudo;
        delete[] matrice_flot2;
        delete[] matrice_imagette_courante;
        delete[] matrice_difference;
        delete[] vecteur_deplacement;
        delete[] gradient_x;
        delete[] gradient_xy;
        delete[] gradient_y;
        return -1;
    }
    //Initialisation de la position du cadre
    x_courant = x_initial;
    y_courant = y_initial;

    StandBy * standBy = new StandBy(&cap);
    camMode = standBy;
    for (ever)
    {
        camMode->exec();
    }

    //Boucle infinie
    for(ever){
        cap >> frame; //R�cup�ration de l'image courante
        flip(frame,frame,0); //Retournement de l'image
        frame_d_affichage = frame.clone(); //Image utilis�e uniquement pour l'affichage
        affiche_cadre(frame_d_affichage, x_courant, y_courant, mlin, mcol, 255, 0, 0); //Ajout du cadre de ciblage � la frame d'affichage
        putText(frame_d_affichage, texte_ecran, cvPoint(30,30),FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0,0,255), 1, CV_AA); //Affichage du mode courant
        imshow(NOM_FENETRE_PRINCIPALE,frame_d_affichage); // Affichage de l'image en cours
        cvtColor(frame,frame,COLOR_BGR2GRAY); // Mise en nuances de gris
        code_clavier = waitKey(25); //V�rifie si on appuie sur une touche du clavier

        //Modes
        if(code_clavier == CLAVIER_ECHAP){ //Arr�t du programme
            cout << "Demande de fin du programme..." << endl;
            pelco.stop(); //Arr�t de la cam�ra
            break;
        }else if(mode == 0){ //Mode contr�le clavier
            if(code_clavier == CLAVIER_ESPACE){
                cout << "Retour a la position initiale" << endl;
                pelco.retour_position_initiale();
                code_clavier = -1;
            }else if(code_clavier == CLAVIER_ENTREE){ //Changement de mode
                cout << "Mode asservissement visuel" << endl;
                cout << "Prise d'un nouveau motif" << endl;
                texte_ecran = "Nouveau motif";
                mode = 1;
                code_clavier = -1;
            }else if(code_clavier == CLAVIER_HAUT){ //Tilt haut
                pelco.pan_tilt_zoom(0,-VITESSE_MOYENNE,0);
                code_clavier = -1;
            }else if(code_clavier == CLAVIER_GAUCHE){ //Pan gauche
                pelco.pan_tilt_zoom(-VITESSE_MOYENNE,0,0);
                code_clavier = -1;
            }else if(code_clavier == CLAVIER_DROITE){ //Pan droite
                pelco.pan_tilt_zoom(VITESSE_MOYENNE,0,0);
                code_clavier = -1;
            }else if(code_clavier == CLAVIER_BAS){ //Pan bas
                pelco.pan_tilt_zoom(0,VITESSE_MOYENNE,0);
                code_clavier = -1;
            }else if(code_clavier == CLAVIER_Z){ //Zoom
                pelco.pan_tilt_zoom(0,0,1);
                code_clavier = -1;
            }else if(code_clavier == CLAVIER_S){ //D�zoom
                pelco.pan_tilt_zoom(0,0,-1);
                code_clavier = -1;
            }else if(code_clavier == CLAVIER_D){ //Focus loin
                pelco.focus(1);
                code_clavier = -1;
            }else if(code_clavier == CLAVIER_Q){ //Focus proche
                pelco.focus(-1);
                code_clavier = -1;
            }
            else if(code_clavier == CLAVIER_C)
            {
                mode = CAMSHIFT_MODE;
                camMode = new Camshift(&cap);
                camMode->init();
            }
            else{
                pelco.stop();
            }
        }else if(mode == 1){ //Mode prise d'un nouveau motif
            motif = frame(Rect(x_initial, y_initial, mcol, mlin)); //On d�coupe le motif dans l'image courante
            imshow(NOM_FENETRE_MOTIF,motif); //On l'affiche
            mat2double(motif, matrice_motif, mlin, mcol); //On la copie dans un tableau de doubles
            gradient_castan(matrice_motif, gradient_x, gradient_y, mlin, mcol, 0.4); //On calcule le gradient selon la m�thode de Shen-Castan
            //Initialisation de la premi�re matrice du calcul de flot optique
            matrice_pseudo[0] = 0.;
            matrice_pseudo[1] = 0.;
            matrice_pseudo[2] = 0.;
            matrice_pseudo[3] = 0.;
            //Remplissage de la premi�re matrice de flot optique
            for(int i=0; i<mlin; i++){
                    for(int j=0; j<mcol; j++){
                        matrice_pseudo[0] += gradient_x[i*mcol+j]*gradient_x[i*mcol+j];
                        matrice_pseudo[1] += gradient_x[i*mcol+j]*gradient_y[i*mcol+j];
                        matrice_pseudo[2] += gradient_x[i*mcol+j]*gradient_y[i*mcol+j];
                        matrice_pseudo[3] += gradient_y[i*mcol+j]*gradient_y[i*mcol+j];
                    }
            }
            // Inversion de la premi�re matrice de flot optique
            int test = pseudo_inverse(matrice_pseudo, matrice_pseudo, 2, 2);
            if(test==0){ //On v�rifie que l'allocation a bien march�
                cout << "Impossible d'allouer de la m�moire pour la pseudo inverse" << endl;
                return -1;
            }
            //On cr�e une image pour afficher le gradient xy
            gradient = motif.clone();
             for(int i = 0; i< mlin*mcol; i++){
                gradient_xy[i] = sqrt((gradient_x[i]*gradient_x[i])+(gradient_y[i]*gradient_y[i]));
             }
            double2mat(gradient,gradient_xy,mlin,mcol);
            gradient = Scalar::all(255) - gradient; //On inverse l'image parce que c'est plus facile � visualiser
            imshow("Gradient",gradient);
            //On r�initialise la position du cadre
            x_courant = x_initial;
            y_courant = y_initial;
            //On initialise les vitesses
            vitesse_pan = 0;
            vitesse_tilt = 0;
            sens_zoom = 0;
            //On initialise l'accumulateur
            accumulateur_x = 0;
            accumulateur_y = 0;
            //On passe au mode suivi de cible
            texte_ecran = "Mode Suivi de cible";
            mode = 2;
        }else if(mode == 2){ //Mode suivi de cible
            if(code_clavier == CLAVIER_ESPACE){ // Retour en mode contr�le clavier par appui sur espace
                cout <<"Mode controle clavier" << endl;
                mode = 0;
                code_clavier = -1;
                //On recentre le cadre
                x_courant = x_initial;
                y_courant = y_initial;
                texte_ecran = "Mode controle clavier";
            }else if(code_clavier == CLAVIER_ENTREE){ //On prend un nouveau motif par appui sur espace
                cout << "Mode asservissement visuel" << endl;
                cout << "Prise d'un nouveau motif" << endl;
                mode = 1;
            }else{
                //On extrait l'imagette courante de l'image courante
                extraire_imagette(frame, matrice_imagette_courante, x_courant, y_courant, mlin, mcol);
                // On fait la diff�rence entre le motif et l'imagette courante
                soustraire_matrices(matrice_motif,matrice_imagette_courante, matrice_difference, mlin, mcol);
                //On affiche la diff�rence
                Mat Im_diff = motif.clone();
                double2mat(Im_diff,matrice_difference,mlin,mcol);
                imshow("Difference",Im_diff);
                //On initialise la deuxi�me matrice du calcul de flot optique
                matrice_flot2[0]=0;
                matrice_flot2[1]=0;
                //On remplit la deuxi�me matrice du flot optique
                for(int i=0; i<mlin; i++){
                    for(int j=0; j<mcol; j++){
                        matrice_flot2[0] += gradient_x[i*mcol+j]*matrice_difference[i*mcol+j];
                        matrice_flot2[1] += gradient_y[i*mcol+j]*matrice_difference[i*mcol+j];
                    }
                }
                //On multiplie les deux matrices du calcul de flot optique
                multiplier_matrices(matrice_pseudo,matrice_flot2,vecteur_deplacement,2,2,2,1);
                //On ajoute le d�placement courant � l'accumulateur
                accumulateur_x += vecteur_deplacement[0];
                accumulateur_y += vecteur_deplacement[1];
                //On met � jour la position du cadre
                x_courant= accumulateur_x + x_initial;
                y_courant= accumulateur_y +y_initial;
                //On limite la position du cadre pour qu'il reste dans l'image
                if (y_courant>(flin-mlin-2)) {y_courant=flin-mlin-2;}
                if (y_courant<1) {y_courant=1;}
                if (x_courant>(fcol-mcol-2)) {x_courant=fcol-mcol-2;}
                if (x_courant<1) {x_courant=1;}
                //On calcule la vitesse � envoyer aux moteurs
                vitesse_pan = lambda*accumulateur_x;
                vitesse_tilt = lambda*accumulateur_y;
                //On envoie la vitesse aux moteurs
                pelco.pan_tilt_zoom(vitesse_pan,vitesse_tilt,sens_zoom);
            }
        }
        
    }
    //Lib�ration de la m�moire
    delete[] matrice_motif;
    delete[] matrice_pseudo;
    delete[] matrice_imagette_courante;
    delete[] matrice_difference;
    delete[] vecteur_deplacement;
    delete[] gradient_x;
    delete[] gradient_xy;
    delete[] gradient_y;
    delete[] matrice_flot2;
    Fin du programme
    return 0;
}
