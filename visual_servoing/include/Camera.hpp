#ifndef CAMERA_H
#define CAMERA_H

#include <opencv2/core/utility.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/video/tracking.hpp"
#include "opencv2/opencv.hpp"


#include <iostream>
#include <ctype.h>

#include "PelcoD.hpp"

using namespace cv;
using namespace std;

class CameraMode;
class Camera {

private:
	CameraMode *cameraMode;
	PelcoD *pelcoD;
	char vpan,vtilt,zoom;
	void init();
	char getVpan();
	char getVtilt();
public:
	Camera();
	explicit Camera(CameraMode * cameraMode);
	virtual void setMode(CameraMode * cameraMode);
	virtual void execMode();
	virtual void setPelcoD(PelcoD * pelcoD);
	void setPanTiltZoom(double dpan , double dtilt , int dzoom);
	virtual PelcoD* getPelcoD();
	virtual void switchCameraMode(CameraMode* cameraMode);
	virtual CameraMode * getCameraMode();
	double getVpand();
	double getVtiltd();

};


#endif
