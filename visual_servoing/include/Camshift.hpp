#ifndef CAMSHIFT_H
#define CAMSHIFT_H

#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/video/tracking.hpp"

#include <iostream>
#include <ctype.h>
#include <iomanip>
#include "Constants.hpp"
#include "operations_flot_optique.hpp"

using namespace cv;
using namespace std;

#include "CameraMode.hpp"


/**
 * @brief      Class for camshift.
 */
class Camshift : public CameraMode{
private:
	
	using super = CameraMode;
	Rect trackWindow;
	bool backprojMode = false;
	
	bool showHist = true;
	int vmin = 10, vmax = 256, smin = 30;
	
	int hsize = 16;
    float hranges[2] = {0,180};
    const float* phranges = hranges;

    Mat frame, hsv, hue, mask, hist, histimg = Mat::zeros(200, 320, CV_8UC3), backproj;
    bool paused = false;

public: 
	Camshift( VideoCapture * cap);
	~Camshift();
	virtual void exec();   
	virtual void init();
	// virtual void servoing_iteration();
	virtual void help();
	virtual void stop();	

	virtual void checkKeys(char key);
};

#endif