#ifndef CAMERA_MODE_H
#define CAMERA_MODE_H

#include <opencv2/core/utility.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/video/tracking.hpp"
#include "opencv2/opencv.hpp"

#include <iostream>
#include <ctype.h>
#include <map>

#include "Camera.hpp"
#include "Constants.hpp"
#include "ImageOperations.hpp"
#include "CameraModeFactory.hpp"

using namespace cv;
using namespace std;


class CameraMode {

private:
	VideoCapture *cap;
	string hot_keys;
	Camera * camera;

public:
	explicit CameraMode(VideoCapture *cap);
	CameraMode(VideoCapture *cap , Camera * camera);
	virtual void help()=0;
	virtual void exec()=0;

	virtual void init ()=0;
	virtual void stop ()=0;

	virtual VideoCapture* getCap();
	virtual string getHot_keys();

	virtual void setHot_keys(const string &hot_keys);
	virtual void setCamera(Camera* camera);

	virtual	void switchCameraMode(CameraModeFactory::CameraModes mode);
	virtual void setPanTiltZoom(double pan , double tilt , int zoom);
	virtual Camera *getCamera();

	virtual void checkZoom(char key);
	// TODO : static virtual method on mouse!
	// virtual static void onMouse( int event, int x, int y, int, void* )=0;
	virtual void checkKeys(char key);
	virtual void checkArrowKeys(char key);
	virtual void frameCorrection(Mat *frame);
};


#endif
