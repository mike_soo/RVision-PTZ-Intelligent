#ifndef STAND_BY_H
#define STAND_BY_H
#include <opencv2/core/utility.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/video/tracking.hpp"
#include "opencv2/opencv.hpp"
#include "CameraMode.hpp"
#include "CameraModeFactory.hpp"
#include "main.hpp"
#include "operations_flot_optique.hpp"
#include <iostream>
#include <ctype.h>

using namespace cv;
using namespace std;



class StandBy: public CameraMode{
private:
  typedef CameraMode super; 
public:
	explicit StandBy(VideoCapture *cap);

	virtual void help();
	virtual void exec();
	virtual void stop();
	virtual void init();
	
	// TODO : static virtual method on mouse!
	// virtual static void onMouse( int event, int x, int y, int, void* )=0;

};


#endif
