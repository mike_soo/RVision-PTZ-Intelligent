#ifndef OPTICALFLOWPYRAMIDAL_HPP
#define OPTICALFLOWPYRAMIDAL_HPP

#include "CameraMode.hpp"

/**
 * \brief Implementation of optical flow with pyramidal method.
 * \details Key presses are handled by OpticalFlowPyramidal::process_input().\n
 *
 *
 *          ENTER for capturing new motif.\n
 *          SPC for stopping track.\n
 *          ESC for quitting.\n
 *
 *
 * Tracking process:
 *
 *
 *          1. define motif region, get motif image
 *          2. get camera frames from update_frame()
 *          3. extract motif region from camera frames
 *          4. calculate the difference between motif initial and current motif image
 *             calculate the displacement
 *
 * See https://docs.google.com/presentation/d/1VisBVzGK9nTC-VnXn2oOnv8wHYhWGhcLMb37w1N2Jes/edit?usp=sharing
 */
class OpticalFlowPyramidal : public CameraMode {
public:
  explicit OpticalFlowPyramidal(cv::VideoCapture* cap, unsigned level=2);

  ~OpticalFlowPyramidal();

  void help() override;
  void exec() override;
  void stop() override;
  void init() override;

  void process_input();

  /**
   * \brief Get camera images, extract motif region of different pyramid level.
   */
  void update_frame();

protected:

  /** An enum type used to switch between different sub-mode.
   *
   */
  enum class Mode {
    CamControl,    /**< Mode controlling camera by keyboard */
    MotifCapture,  /**< Mode capturing a new pattern (small area of image) */
    Tracking       /**< Mode tracking */
  };

private:
  /**
   * \brief Variable used to control current mode loop
   */
  bool quit;

  /**
   * \brief Current mode.
   * \details Mode::CamControl, Mode::MotifCapture, Mode::Tracking
   */
  Mode mode;

  /**
   * \brief Level of pyramid images.
   *
   * \details In the basic optical flow method, level = 1.
   */
  unsigned level;

  /**@{*/
  /**
   * \brief Accumulator to sum the displacement in all pyramid images levels.
   *
   * \details \f$\vec{v} = \sum^{L_n}_{L=0}2^L\vec{v}^L\f$
   */

  double acc_x;
  double acc_y;
  /**@}*/

  /**@{*/
  /**
   * \brief Camera pan/tilt speed
   *
   * \details Value between -0x3f(-65) to 0x3f(65)
   */

  double pan_speed;
  double tilt_speed;
  /**@}*/

  /**
   * \brief Tracking speed factor.
   */
  double lambda;

  /**
   * \brief Text displayed on the screen.
   *
   * \details Just a text to notify which mode we are now.
   */
  std::string text;

  /**
   * \brief Used to create file name string.
   *
   * \details Creating file names automatically when saving current frame images.
   * <a href="http://www.cplusplus.com/reference/sstream/stringstream/">More info.</a>
   */
  std::stringstream ssm;

  /**
   * \brief Vector stores all camera original images of different pyramid level, used only for display.
   *
   * \details Text, motif regions are all drew on the images stored in this vector.
   */
  std::vector<cv::Mat> frames;

  /**
   * \brief A vector stores all camera original images of different pyramid level (converted in gray-level).
   *
   * See: OpticalFlowPyramidal::update_frame()
   */
  std::vector<cv::Mat> gray_frames;


  /**@{*/
  /**
   * \brief Image frames of different level on which we do the image treatment.
   *
   * \detail They are the same with OpticalFlowPyramidal::frames.
   */
  std::vector<cv::Mat> pyr_frames;

  /**
   * \brief Image frames of different level on which we do the image treatment.
   *
   * \detail They are the same with OpticalFlowPyramidal::gray_frames.
   */
  std::vector<cv::Mat> pyr_gray_frames;

  /**
   * \brief Motif image extracted from OpticalFlowPyramidal::pyr_gray_frames.
   */
  std::vector<cv::Mat> pyr_motifs;

  /**
   * \brief Gradient image of motifs.
   *
   * \details Calculated using Castan method.
   */
  std::vector<cv::Mat> pyr_motifs_grad;

  /**
   * \brief Used to save current motif image.
   */
  std::vector<cv::Mat> pyr_imagettes;
  /**@}*/

  /**
   * \brief Matrix stocking the difference between the motif and current image region.
   */
  std::vector<cv::Mat> pyr_motifs_diff;

  /**
   * \brief Size of motifs of different level of pyramid images.
   */
  std::vector<cv::Size> pyr_motif_sizes;

  /**
   * \brief Motifs' initial position relative to each level of pyramid images.
   */
  std::vector<cv::Point> pyr_motif_init_poss;

  /**
   * \brief Structure saving current motif region's position and size.
   */
  std::vector<cv::Rect> pyr_motif_rects;

  /**
   * \name Same structure with double typed array representation.
   */
  /**@{*/
  std::vector<double*> pyr_motif_mats;
  std::vector<double*> pyr_imagette_mats;
  std::vector<double*> pyr_motif_diff_mats;
  /**@}*/

  /**
   * \name Structure used to calculate motif gradient.
   */
  /**@{*/
  std::vector<double*> pyr_motif_grad_x_mats;
  std::vector<double*> pyr_motif_grad_y_mats;
  std::vector<double*> pyr_motif_grad_xy_mats;
  /**@}*/


  /**
   * \name Matrices used to solve over-dertermined linear equation.
   */
  /**@{*/
  std::vector<double*> pyr_pseudo_mats;
  std::vector<double*> pyr_flot2_mats;
  std::vector<double*> pyr_displacement_mats;
  /**@}*/
};

#endif  //OPTICALFLOWPYRAMIDAL_HPP
