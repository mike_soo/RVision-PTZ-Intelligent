#ifndef CAMERA_MODE_FACTORY_H
#define CAMERA_MODE_FACTORY_H

#include <opencv2/core/utility.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/video/tracking.hpp"
#include "opencv2/opencv.hpp"



#include <iostream>
#include <ctype.h>
#include <map>

using namespace cv;
using namespace std;

class Camera;
class CameraMode;
class CameraModeFactory {

private:
	VideoCapture *cap;
	string hot_keys;
	Camera * camera;
public:
	enum CameraModes{
		CAMSHIFT,
		STANDBY,
    	BASICOF,
    	PYROF,
    	END
	};
	static map<CameraModeFactory::CameraModes,CameraMode*> * cameraModeInstances ;
	static CameraMode* get(CameraModeFactory::CameraModes mode);
	static CameraMode* cameraModeFactoryGetElement(CameraModeFactory::CameraModes mode);
	static void init(VideoCapture *cap);
};


#endif
