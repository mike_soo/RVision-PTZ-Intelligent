#ifndef IMAGE_OPERATIONS_H
#define IMAGE_OPERATIONS_H

#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/video/tracking.hpp"

#include <iostream>
#include <ctype.h>
#include <iomanip>

using namespace cv;
using namespace std;


class ImageOperations
{
public:
	static Mat rotate(Mat src, double angle);
	static void makeNbOfRowsOdd(Mat * mat);
	static void makeNbOfColsOdd(Mat * mat);
};

#endif