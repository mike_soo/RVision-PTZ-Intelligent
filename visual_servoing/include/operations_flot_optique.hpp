/**
    @file operations_flot_optique.hpp
    @brief Diverses fonctions d'opérations sur les matrices nécessaire au flot optique
    @author Tangi Pointeau et Albane Ordrenneau
    @version 2.1
    @date 07/02/2018
*/

#ifndef OPERATIONS_FLOT_OPTIQUE_H
#define OPERATIONS_FLOT_OPTIQUE_H

#include <iostream>
#include <cmath>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include "gradient_castan.hpp"

void mat2double(cv::Mat image, double *tableau, int lin, int col);
void double2mat(cv::Mat image, double *tableau, int lin, int col);
// void affiche_cadre(cv::Mat image, int x, int y, int lin, int col, int rouge, int vert, int bleu);
void affiche_cadre(cv::Mat &image, int x, int y, int lin, int col, int rouge, int vert, int bleu);
void extraire_imagette(cv::Mat image, double *tableau, int x, int y, int lin, int col);
void remplir_identite(double *tableau, int dim);
void copier_matrice(double *matrice_source, double *matrice_cible, int lin, int col);
void transposer_matrice(double *matrice_in, double *matrice_out, int lin,int col);
void multiplier_matrices(double *matrice_in_gauche, double *matrice_in_droite, double *matrice_out, int lin1, int col1, int lin2, int col2);
void inverser_matrice(double *matrice_in, double *matrice_out, int dim);
void soustraire_matrices(double *matrice_in_gauche, double *matrice_in_droite, double *matrice_out, int lin, int col);
int pseudo_inverse(double *matrice_in,double *matrice_out,int lin, int col);


#endif // OPERATIONS_FLOT_OPTIQUE_H
