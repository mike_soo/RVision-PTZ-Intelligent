#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

/**
	@file main.hpp
    @brief Header qui fournit les macros utilisees dans le main
    @author Tangi Pointeau et Albane Ordrenneau
    @version 2
    @date 15/02/2018
*/


#define NOM_FENETRE_PRINCIPALE "Camera PTZ Hikvision" // Nom de la fenêtre principale 
#define NOM_FENETRE_MOTIF "Motif a suivre" // Nom de la fenêtre dans laquelle on affiche le motif 
#define ADRESSE_DIP_CAMERA 0x1e // Adresse de la caméra fixée avec les DIP switch 

//Vitesses prédéfinies pour le contrôle au clavier
#define VITESSE_LENTE 0x1f // Vitesse lente du pan-tilt 
#define VITESSE_MOYENNE 0x2f // Vitesse moyenne du pan-tilt 
#define VITESSE_RAPIDE 0x3f // Vitesse maximum du pan-tilt 

//Tailles des images
#define flin frame.rows // Nombre de lignes de l'image principale 
#define fcol frame.cols //Nombre de colonnes de l'image principale 
#define mlin flin/3 // Nombre de lignes du motif 
#define mcol fcol/3 // Nombre de colonnes du motif 

//Position initiale du coin supérieure gauche du cadre
#define x_initial (fcol-mcol)/2 // Position horizontale initiale du coin supérieur gauche du cadre 
#define y_initial (flin-mlin)/2 // Position verticale initiale du coin supérieur gauche du cadre 

#define ever ;;

//Codes touches pour le clavier
#ifdef __linux__ //Codes clavier pour Linux
	#define CLAVIER_HAUT 82 // Code de la touche flèche haut du clavier sur Linux 
	#define CLAVIER_BAS 84 // Code de la touche flèche bas du clavier sur Linux 
	#define CLAVIER_DROITE 83 // Code de la touche flèche droite du clavier sur Linux 
	#define CLAVIER_GAUCHE 81 // Code de la touche flèche gauche du clavier sur Linux 
	#define CLAVIER_ENTREE 13 // Code de la touche Entrée du clavier sur Linux 
	#define CLAVIER_1 49 // Code de la touche 1 du clavier sur Linux 
	#define CLAVIER_2 50 // Code de la touche 2 du clavier sur Linux 
	#define CLAVIER_3 51 // Code de la touche 2 du clavier sur Linux 
	#define CLAVIER_ESPACE 32
	#define ZOOM_IN 43
	#define ZOOM_OUT 45
#elif _WIN32 //Codes clavier pour Windows
	#define CLAVIER_HAUT 2490368 // Code de la touche flèche haut du clavier sur Windows 
	#define CLAVIER_BAS 2621440 // Code de la touche flèche bas du clavier sur Windows 
	#define CLAVIER_DROITE 2555904 // Code de la touche flèche droite du clavier sur Windows 
	#define CLAVIER_GAUCHE 2424832 // Code de la touche flèche gauche du clavier sur Windows 
	#define CLAVIER_ENTREE 13 // Code de la touche Entrée du clavier sur Windows 
	#define CLAVIER_1 49 // Code de la touche 1 du clavier sur Windows 
	#define CLAVIER_2 50 // Code de la touche 2 du clavier sur Windows 
	#define CLAVIER_3 51 // Code de la touche 3 du clavier sur Windows 
#endif
#define CLAVIER_ECHAP 27 // Code clavier de la touche échap 
#define CLAVIER_ESPACE 32 // Code clavier de la touche espace 
#define CLAVIER_Z 122 // Code clavier de la touche z 
#define CLAVIER_S 115 // Code clavier de la touche s 
#define CLAVIER_Q 113 // Code clavier de la touche q 
#define CLAVIER_D 100 // Code clavier de la touche d 
#define CLAVIER_C 99  // Code clavier de la touche c

#define CAMSHIFT_MODE 'c'

#endif